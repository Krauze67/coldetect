package coldetect

import (
	"strings"
)

type TResSortByScoreDesc []TDetectResInt

func (t TResSortByScoreDesc) Len() int {
	return len(t)
}
func (t TResSortByScoreDesc) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}
func (t TResSortByScoreDesc) Less(i, j int) bool {
	//First - by coeff calculated
	if 0 != t[i].Score || 0 != t[j].Score {
		return t[i].Score > t[j].Score
	}
	//then by countries weigth
	if t[i].Country.CountrySpecialInfo.CountryWeight > t[j].Country.CountrySpecialInfo.CountryWeight {
		return true
	}
	//then by country name
	return -1 == strings.Compare(t[i].Country.CountryName, t[j].Country.CountryName)
}
