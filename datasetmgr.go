package coldetect

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"io"

	"gitlab.com/Krauze67/flib/filetools"
	"gitlab.com/Krauze67/flib/slicetools"
	"gitlab.com/Krauze67/flib/stringtools"
)

const (
	cFileProcessingLogPrefix = "_Log_"
	cWorkLogHdr              = "oper#,"

	cFileExtAllowed = ".csv"

	cDSContentCommentChar = '#'
	cDSContentNameViewSep = "|"
	cDSContentGenderF     = 'F'
	cDSContentGenderM     = 'M'
	cDSContentGenderU     = 'U'

	cTrimTotals         = "\n\r\t ,:\""
	cDSContentValTotal  = "TOTAL"
	cDSContentValTotalM = "TOTALM"
	cDSContentValTotalF = "TOTALF"
	//Old one constants
	// Total births:,1658699,,,
	// Total M:,895801,,,
	// Total F:,762898,,,
	cDSContentValTotal_Old         = "TOTAL BIRTHS"
	cDSContentValTotalM_Old        = "TOTAL M"
	cDSContentValTotalF_Old        = "TOTAL F"
	cDSContentPrecisionFloat       = "9"
	cDSContentPrecisionNoCountPerc = "3"
	cDSContentPrecisionNoCountProp = "1"

	cTblDelimiter                = ","
	cTblMinFieldsCount           = 4
	cTblHdrName           string = "NAME"
	cTblHdrGender         string = "GENDER"
	cTblHdrCount          string = "COUNT"
	cTblHdrPercent        string = "PERCENT"
	cTblHdrProp100k       string = "PROP100K"
	cTblHdrIsNoRealCount  string = "ISNOCOUNT"
	cTblHdrIsNoRealCount2 string = "ISNOTREALCOUNT"
	cTblHdrComment        string = "COMMENT"
	cTblHdrAll                   = cTblHdrName + "," + cTblHdrGender + "," +
		cTblHdrCount + "," + cTblHdrPercent + "," + cTblHdrProp100k + "," +
		cTblHdrIsNoRealCount + "," + cTblHdrComment

	cFilenamePartIsNoCount = "NOCOUNT"

	defUnrankedCount    = 1
	defUnrankedPercent  = 0.001
	defUnrankedProp100k = 1
)

type TDsSubset string

const (
	TDsSubsetFirstname = "firstname"
	TDsSubsetLastname  = "lastname"
	TDsSubsetMidname   = "middlename"
)

func (t *TDsSubset) String() string {
	if *t == TDsSubsetFirstname {
		return "firstname"
	} else if *t == TDsSubsetLastname {
		return "lastname"
	}
	panic("invalid TDsSubsetLastname value")
	return ""
}

// TNameFullInfo contains a name(s) their count of total and their approx. count by 100k of population
type TNameFullInfo struct {
	LineNum    int64
	Commented  bool
	Name       []string
	Gender     rune
	Count      int64
	Percent    float64
	Prop100k   float64
	IsUnranked bool
	Comment    string
}

type TSingleDS struct {
	Subset       TDsSubset
	Filename     string
	IsRanked     bool
	LinesTotal   int64
	FileComments map[int64]string
	Total        int64
	TotalM       int64
	TotalF       int64
	NameInfoArr  []*TNameFullInfo
	NameInfoMap  map[string]*TNameFullInfo //for fast-access and unique-only save

	Log *TDsMgrLog

	//fields indexes
	idxName       int
	idxGender     int
	idxCount      int
	idxPercent    int
	idxProp100K   int
	idxIsFakeRank int
	idxComment    int
}

type TCountryNameDS struct {
	//Country    string
	Subset     TDsSubset
	DSFullPath string

	RankedDS   []*TSingleDS
	UnrankedDS []*TSingleDS

	NameInfoMap map[string]*TNameFullInfo //for fast-access

	Log *TDsMgrLog
}

type TDsManager struct {
}

func FixAndRejoinDatasest(dsroot string, country string, subset TDsSubset, extLogLevel log.Level, fileLogLevel log.Level) error {
	return TDsManager{}.FixAndRejoinDatasestInt(dsroot, country, subset, extLogLevel, fileLogLevel)
}

func AddFileToDataset(fullfilename string, dsroot string, country string, subset TDsSubset, extLogLevel log.Level, fileLogLevel log.Level) error {
	return TDsManager{}.AddFileToDatasetInt(fullfilename, dsroot, country, subset, extLogLevel, fileLogLevel)
}

func (t TDsManager) FixAndRejoinDatasestInt(dsroot string, country string, subset TDsSubset, extLogLevel log.Level, fileLogLevel log.Level) error {
	log.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) called", dsroot, country, subset.String())
	log.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) creating a new log-file for processing", dsroot, country, subset.String())
	dsFullPath := filepath.Join(dsroot, country, subset.String())
	timestamp := time.Now()
	logger := t.getNewLog(dsFullPath, timestamp, extLogLevel, fileLogLevel)
	log.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) new processing log-file created", dsroot, country, subset.String())

	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) called", dsroot, country, subset.String())
	//Check directories:
	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - check directory exist", dsroot, country, subset.String())
	_, err := os.Stat(dsroot)
	if nil != err {
		logger.Errorf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - directory does not exist %v", dsroot, country, subset.String(), err)
		return err
	}
	logger.Debugf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - directory exist", dsroot, country, subset.String())

	logger.Debugf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - check sub-dir exist", dsroot, country, subset.String())
	//dsFullPath := filepath.Join(dsroot, country, subset.String())
	if _, err = os.Stat(dsFullPath); nil != err {
		logger.Errorf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - sub-dir does not exist %v", dsroot, country, subset.String(), err)
		return err
	}
	logger.Debugf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - sub-dir exist", dsroot, country, subset.String())

	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - start backup", dsroot, country, subset.String())
	bckpDir := ""
	if bckpDir, err = t.backupAll(dsFullPath, timestamp); nil != err {
		logger.Errorf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - backup error %v", dsroot, country, subset.String(), err)
		return err
	}
	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - backup done to %s", dsroot, country, subset.String(), bckpDir)

	DSCountryName := TCountryNameDS{DSFullPath: dsFullPath, Subset: subset, Log: logger}
	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - start to load datasets", dsroot, country, subset.String())
	if err = DSCountryName.loadCurrentDatasets(); nil != err {
		logger.Errorf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - load datasets failed: %v", dsroot, country, subset.String(), err)
		return err
	}

	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - datasets loaded ok", dsroot, country, subset.String())

	//reconstruct and join all names with their another views inside the same DS:
	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - Ranked datasets reconstruction started", dsroot, country, subset.String())
	for _, pDs := range DSCountryName.RankedDS {
		pDs.reconstructNamesMapAndJoinNameInfo()
	}
	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - Unranked datasets reconstruction started", dsroot, country, subset.String())
	for _, pDs := range DSCountryName.UnrankedDS {
		pDs.reconstructNamesMapAndJoinNameInfo()
	}

	//now merge all info got
	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - Merge all datasets loaded to a single nameView->Info map", dsroot, country, subset.String())
	DSCountryName.mergeAllDsToMap()

	//Save all corrected infos:
	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - Saving datasets corrected - ranked", dsroot, country, subset.String())
	for _, pDs := range DSCountryName.RankedDS {
		fullfilename := pDs.getFileNameCorrected("")
		if err := pDs.SaveToFile(fullfilename, true); nil != err {
			logger.Errorf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - error[%s] saving dataset %s", dsroot, country, subset.String(), err.Error(), pDs.Filename)
		}
	}
	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - Saving datasets corrected - unranked", dsroot, country, subset.String())
	for _, pDs := range DSCountryName.UnrankedDS {
		fullfilename := pDs.getFileNameCorrected("")
		if err := pDs.SaveToFile(fullfilename, true); nil != err {
			logger.Errorf("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - error[%s] saving dataset %s", dsroot, country, subset.String(), err.Error(), pDs.Filename)
		}
	}

	logger.Infof("TDsManager.FixAndRejoinDatasest(%s, %s, %s) - fixes and re-join has been successfully finished", dsroot, country, subset.String())
	return nil
}

func (t TDsManager) AddFileToDatasetInt(fullfilename string, dsroot string, country string, subset TDsSubset, extLogLevel log.Level, fileLogLevel log.Level) error {
	log.Infof("TDsManager.AddFileToDataset(%s, %s, %s) called", dsroot, country, subset.String())
	log.Infof("TDsManager.AddFileToDataset(%s, %s, %s) creating a new log-file for processing", dsroot, country, subset.String())
	dsFullPath := filepath.Join(dsroot, country, subset.String())
	timestamp := time.Now()
	logger := t.getNewLog(dsFullPath, timestamp, extLogLevel, fileLogLevel)
	log.Infof("TDsManager.AddFileToDataset(%s, %s, %s) new processing log-file created", dsroot, country, subset.String())

	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) called", dsroot, country, subset.String())
	//Check directories:
	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - check directory exist", dsroot, country, subset.String())
	_, err := os.Stat(dsroot)
	if nil != err {
		logger.Errorf("TDsManager.AddFileToDataset(%s, %s, %s) - directory does not exist %v", dsroot, country, subset.String(), err)
		return err
	}
	logger.Debugf("TDsManager.AddFileToDataset(%s, %s, %s) - directory exist", dsroot, country, subset.String())

	logger.Debugf("TDsManager.AddFileToDataset(%s, %s, %s) - check sub-dir exist", dsroot, country, subset.String())
	//dsFullPath := filepath.Join(dsroot, country, subset.String())
	if _, err = os.Stat(dsFullPath); nil != err {
		logger.Errorf("TDsManager.AddFileToDataset(%s, %s, %s) - sub-dir does not exist %v", dsroot, country, subset.String(), err)
		return err
	}
	logger.Debugf("TDsManager.AddFileToDataset(%s, %s, %s) - sub-dir exist", dsroot, country, subset.String())

	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - start backup", dsroot, country, subset.String())
	bckpDir := ""
	if bckpDir, err = t.backupAll(dsFullPath, timestamp); nil != err {
		logger.Errorf("TDsManager.AddFileToDataset(%s, %s, %s) - backup error %v", dsroot, country, subset.String(), err)
		return err
	}
	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - backup done to %s", dsroot, country, subset.String(), bckpDir)

	dsCountryName := TCountryNameDS{DSFullPath: dsFullPath, Subset: subset, Log: logger}
	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - start to load datasets", dsroot, country, subset.String())
	if err = dsCountryName.loadCurrentDatasets(); nil != err {
		logger.Errorf("TDsManager.AddFileToDataset(%s, %s, %s) - load datasets failed: %v", dsroot, country, subset.String(), err)
		return err
	}
	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - datasets loaded ok", dsroot, country, subset.String())

	//Create a new DS to add:
	pDsToAdd := dsCountryName.createSingleDataset(fullfilename, dsCountryName.Subset)
	if nil == pDsToAdd {
		err = fmt.Errorf("TDsManager.AddFileToDataset(%s, %s, %s) - failed to load dataset from [%s] to add", dsroot, country, subset.String(), fullfilename)
		logger.Errorln(err.Error())
		return err
	}

	//now merge all info got
	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - Merge all datasets loaded to a single nameView->Info map", dsroot, country, subset.String())

	if pDsToAdd.IsRanked {
		dsCountryName.NameInfoMap = make(map[string]*TNameFullInfo)
		dsCountryName.mergeRankedDsToMap()
		dsCountryName.addDsContentToMap(pDsToAdd)
		dsCountryName.RankedDS = append(dsCountryName.RankedDS, pDsToAdd)
		dsCountryName.mergeUnrankedDsToMap()
	} else {
		dsCountryName.mergeAllDsToMap()
		dsCountryName.addDsContentToMap(pDsToAdd)
		dsCountryName.UnrankedDS = append(dsCountryName.UnrankedDS, pDsToAdd)
	}

	//Save all corrected infos:
	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - Saving datasets - ranked", dsroot, country, subset.String())
	for _, pDs := range dsCountryName.RankedDS {
		fullfilename := pDs.getFileNameCorrected(dsCountryName.DSFullPath)
		if err := pDs.SaveToFile(fullfilename, pDsToAdd != pDs); nil != err {
			logger.Errorf("TDsManager.AddFileToDataset(%s, %s, %s) - error[%s] saving dataset %s", dsroot, country, subset.String(), err.Error(), pDs.Filename)
		}
	}
	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - Saving datasets - unranked", dsroot, country, subset.String())
	for _, pDs := range dsCountryName.UnrankedDS {
		fullfilename := pDs.getFileNameCorrected(dsCountryName.DSFullPath)
		if err := pDs.SaveToFile(fullfilename, pDsToAdd != pDs); nil != err {
			logger.Errorf("TDsManager.AddFileToDataset(%s, %s, %s) - error[%s] saving dataset %s", dsroot, country, subset.String(), err.Error(), pDs.Filename)
		}
	}

	logger.Infof("TDsManager.AddFileToDataset(%s, %s, %s) - successfully added new dataset from %s", dsroot, country, subset.String(), fullfilename)
	return nil
}

func (t TDsManager) getTimestampStr(timestamp time.Time) string {
	return fmt.Sprintf("%d%02d%02d-T%02d%02d%02d",
		timestamp.Year(), timestamp.Month(), timestamp.Day(),
		timestamp.Hour(), timestamp.Minute(), timestamp.Second())
}

func (t TDsManager) getBackupDirName(timestamp time.Time) string {
	return fmt.Sprintf("BCKP_%s", t.getTimestampStr(timestamp))
}

func (t TDsManager) backupAll(dsfullpath string, timestamp time.Time) (dircreated string, err error) {
	//Check directories:
	bckpFullPath := filepath.Join(dsfullpath, t.getBackupDirName(timestamp))

	if err = filetools.CopyFolder(dsfullpath, bckpFullPath, 0); nil != err {
		return "", err
	}
	dircreated = bckpFullPath
	return dircreated, nil
}

func (t TDsManager) getNewLog(dsFullPath string, timestamp time.Time, extLogLevel log.Level, fileLogLevel log.Level) *TDsMgrLog {
	newlog := &TDsMgrLog{ExtLogLevel: extLogLevel, FileLogLevel: fileLogLevel}
	logFileName := filepath.Join(dsFullPath, fmt.Sprintf(cFileProcessingLogPrefix+"%s", t.getTimestampStr(timestamp)+".log"))
	var err error
	newlog.File, err = os.Create(logFileName)
	if nil != err {
		log.Errorf("Cannot create a new log-file %s", logFileName)
		return nil
	}
	log.Infof("Log file created: %s", logFileName)
	return newlog
}

func (t *TCountryNameDS) loadCurrentDatasets() error {
	t.Log.Debug("TCountryNameDS.loadCurrentDatasets() called")
	if nil == t {
		panic("TCountryNameDS.loadCurrentDatasets called with nil 'this'")
	}

	var err error
	directory, _ := os.Open(t.DSFullPath)
	objects, err := directory.Readdir(-1)
	t.Log.Infof("TCountryNameDS.loadCurrentDatasets() reading directory %s", t.DSFullPath)
	for _, nextfile := range objects {
		t.Log.Infof("    TCountryNameDS.loadCurrentDatasets() next object is %s", nextfile.Name())
		if nextfile.IsDir() {
			t.Log.Infof("    TCountryNameDS.loadCurrentDatasets() next object %s is dir - skip", nextfile.Name())
			continue
		}
		//skip log-files
		if strings.HasPrefix(nextfile.Name(), cFileProcessingLogPrefix) {
			t.Log.Infof("    TCountryNameDS.loadCurrentDatasets() next file %s is log-file - skip", nextfile.Name())
			continue
		}
		//skip files with not allowed extensions
		if !strings.HasSuffix(nextfile.Name(), cFileExtAllowed) {
			t.Log.Warningf("    TCountryNameDS.loadCurrentDatasets() next file %s had not allowed extension - skip", nextfile.Name())
			continue
		}
		t.Log.Debugf("    TCountryNameDS.loadCurrentDatasets() create next file's[%s] single dataset", nextfile.Name())
		currDS := TSingleDS{
			Subset:       t.Subset,
			Filename:     filepath.Join(t.DSFullPath, nextfile.Name()),
			IsRanked:     false,
			LinesTotal:   0,
			FileComments: make(map[int64]string),
			NameInfoArr:  nil,
			NameInfoMap:  make(map[string]*TNameFullInfo),

			Log: t.Log,

			// fields indexes
			idxName:       -1,
			idxGender:     -1,
			idxCount:      -1,
			idxPercent:    -1,
			idxProp100K:   -1,
			idxIsFakeRank: -1,
			idxComment:    -1,
		}
		t.Log.Infof("    TCountryNameDS.loadCurrentDatasets() next file's[%s] single dataset loading start", nextfile.Name())
		err = currDS.LoadFromFile(filepath.Join(t.DSFullPath, nextfile.Name()))
		if err != nil {
			t.Log.Errorf("    TCountryNameDS.loadCurrentDatasets() next file's[%s] single dataset load failed: %v", nextfile.Name(), err)
			continue
		}

		if currDS.IsRanked {
			t.RankedDS = append(t.RankedDS, &currDS)
		} else {
			t.UnrankedDS = append(t.UnrankedDS, &currDS)
		}

		t.Log.Infof("    TCountryNameDS.loadCurrentDatasets() next file's[%s] single dataset loaded", nextfile.Name())
	}

	t.Log.Infof("TCountryNameDS.loadCurrentDatasets() finished")
	return nil
}

func (t *TCountryNameDS) createSingleDataset(fullfilename string, subset TDsSubset) *TSingleDS {

	//skip files with not allowed extensions
	if !strings.HasSuffix(fullfilename, cFileExtAllowed) {
		t.Log.Errorf("TCountryNameDS.createSingleDataset() file %s has not allowed extension - error loading", fullfilename)
		return nil
	}
	t.Log.Debugf("TCountryNameDS.createSingleDataset() create file's[%s] single dataset", fullfilename)
	pCurrDS := &TSingleDS{
		Subset:       subset,
		Filename:     fullfilename,
		IsRanked:     false,
		LinesTotal:   0,
		FileComments: make(map[int64]string),
		NameInfoArr:  nil,
		NameInfoMap:  make(map[string]*TNameFullInfo),

		Log: t.Log,

		// fields indexes
		idxName:       -1,
		idxGender:     -1,
		idxCount:      -1,
		idxPercent:    -1,
		idxProp100K:   -1,
		idxIsFakeRank: -1,
		idxComment:    -1,
	}
	t.Log.Infof("TCountryNameDS.createSingleDataset() file's[%s] single dataset loading start", fullfilename)
	if err := pCurrDS.LoadFromFile(fullfilename); err != nil {
		t.Log.Errorf("TCountryNameDS.createSingleDataset() file's[%s] single dataset load failed: %v", fullfilename, err)
		return nil
	}

	t.Log.Infof("TCountryNameDS.createSingleDataset() file's[%s] single dataset loaded", fullfilename)
	return pCurrDS
}

func (t *TCountryNameDS) mergeRankedDsToMap() {
	t.Log.Infoln("TCountryNameDS.mergeRankedDsToMap() add all loaded nameInfos to a Dataset's map...")
	//Add all infos to map of name views
	for _, pDs := range t.RankedDS {
		t.addDsContentToMap(pDs)
	} // loop by Ranked datasets
	t.Log.Infoln("TCountryNameDS.mergeRankedDsToMap() - done")
}

func (t *TCountryNameDS) mergeUnrankedDsToMap() {
	t.Log.Infoln("TCountryNameDS.mergeUnrankedDsToMap() add all loaded nameInfos to a Dataset's map...")
	//now add from unranked datasets:
	for _, pDs := range t.UnrankedDS {
		t.addDsContentToMap(pDs)
	} // loop by Ranked datasets
	t.Log.Infoln("TCountryNameDS.mergeUnrankedDsToMap() - done")
}

func (t *TCountryNameDS) mergeAllDsToMap() {
	t.Log.Infoln("TCountryNameDS.mergeAllDsToMap() add all loaded nameInfos to a Dataset's map...")
	t.NameInfoMap = make(map[string]*TNameFullInfo)
	//Add all infos to map of name views
	t.mergeRankedDsToMap()
	//now add from unranked datasets:
	t.mergeUnrankedDsToMap()
	t.Log.Infoln("TCountryNameDS.mergeAllDsToMap() - done")
}

func (t *TCountryNameDS) addDsContentToMap(pDs *TSingleDS) {
	t.Log.Infoln("TCountryNameDS.addDsContentToMap() add Dataset to a map...")
	//Add
	if pDs.IsRanked {
		for _, pCurrInfo := range pDs.NameInfoArr {
			for _, nameView := range pCurrInfo.Name {
				if pExistInfo, ok := t.NameInfoMap[nameView]; ok {
					//
					if pExistInfo.Prop100k < pCurrInfo.Prop100k {
						//Mark previously added info to remove:
						pExistInfo.Commented = true
						//add existent views to InfoToReplaceBy
						slicetools.AppendUniqueString(&pCurrInfo.Name, pExistInfo.Name)
						for _, replaceAddKey := range pCurrInfo.Name {
							t.NameInfoMap[replaceAddKey] = pCurrInfo
						}
					} else {
						pCurrInfo.Commented = true
					}
				} else {
					// if nameView != pCurrInfo.Name[0] {
					// 	t.Log.Info("")
					// }
					t.NameInfoMap[nameView] = pCurrInfo
				} //if(already exist)-else
			} //loop by nameInfo views
		} //loop by all name Infos
	} else {
		for _, pCurrInfo := range pDs.NameInfoArr {
			for _, nameView := range pCurrInfo.Name {
				if pExistInfo, ok := t.NameInfoMap[nameView]; ok {
					slicetools.AppendUniqueString(&pExistInfo.Name, pCurrInfo.Name)
					pCurrInfo.Commented = true
				} else {
					t.NameInfoMap[nameView] = pCurrInfo
				} //if(already exist)-else
			} //loop by nameInfo views
		} //loop by all name Infos
	} // if(Ranked)-Else
	t.Log.Infoln("TCountryNameDS.addDsContentToMap() - done")
}

func (t *TSingleDS) LoadFromFile(fullfilepath string) error {
	if nil == t {
		panic("TSingleDS.LoadFromFile called with nil 'this'")
	}
	t.Log.Debugf("TSingleDS.LoadFromFile(%s) called", fullfilepath)

	file, err := os.Open(fullfilepath)
	if nil != err {
		t.Log.Errorf("TSingleDS.LoadFromFile(%s) file open error [%v]", fullfilepath, err)
		return err
	}
	t.Log.Debugf("TSingleDS.LoadFromFile(%s) source file opened", fullfilepath)
	scanner := bufio.NewScanner(bufio.NewReader(file))
	var nLine int64 = -1
	nNamesLoaded := 0
	var nDataLines, nPercent0, nProp100k0 int64
	strAllTrim := "\n\r\t ," + cTblDelimiter
	var nCountedTotal, nCountedTotalM, nCountedTotalF int64
	//
	bIsAllRatesEqual := true
	var fAllPercentVal, fAllProp100kVal float64 = -1, -1
	// 1 - wait totals
	// 2 - wait headers
	// 3 - read data
	bWaitTotals := true
	bWaitHeaders := true
	bContinueLoad := true
	bTotalsNotPresent := true
	for scanner.Scan() && bContinueLoad {
		nLine++
		t.Log.Debugf("TSingleDS.LoadFromFile(%s) read line %d", fullfilepath, nLine)
		line := strings.ToUpper(strings.Trim(scanner.Text(), strAllTrim))
		if 0 == len(line) {
			t.Log.Debugf("TSingleDS.LoadFromFile(%s) empty line #%d", fullfilepath, nLine)
			continue
		}
		//collect comments if found:
		if cDSContentCommentChar == line[0] {
			t.Log.Debugf("TSingleDS.LoadFromFile(%s) comment line #%d found", fullfilepath, nLine)
			t.FileComments[nLine] = line
			continue
		}

		if bWaitTotals {
			t.Log.Debugf("TSingleDS.LoadFromFile(%s) totals line(s) expecting", fullfilepath)
			parse_res_totals := t.loadFromFile_ParseTotals(line)

			if t.Subset == TDsSubsetLastname {
				bWaitTotals = (0 == t.Total || 0 == t.TotalM || 0 == t.TotalF)
			}

			if bWaitTotals {
				t.Log.Debugf("TSingleDS.LoadFromFile(%s) still waiting for total(s)", fullfilepath)
			} else {
				t.Log.Infof("TSingleDS.LoadFromFile(%s) all totals are parsed", fullfilepath)
				bTotalsNotPresent = false
			}

			if parse_res_totals {
				bTotalsNotPresent = false
				t.Log.Debugf("TSingleDS.LoadFromFile(%s) some of total value parsed", fullfilepath)
				continue
			}
		}

		//headers
		if bWaitHeaders {
			t.Log.Debugf("TSingleDS.LoadFromFile(%s) wait for headers line", fullfilepath)

			headers_parsed := t.loadFromFile_ParseHeaders(line)
			if 0 >= headers_parsed {
				t.Log.Warningf("TSingleDS.LoadFromFile(%s) line #%d reached. Headers has not been recognized", fullfilepath, nLine)
				continue
			} else { // at least one header recognized
				//check if still wait for totals line:
				if bWaitTotals && strings.Contains(line, cTblHdrProp100k) {
					t.Log.Warningf("TSingleDS.LoadFromFile(%s) headers line #%d reached when Total(s) are still expected. Do not wait for Totals line(s) any more", fullfilepath, nLine)
					bWaitTotals = false
				}
			}
			bWaitHeaders = false

			bIncompleteSet := (0 > t.idxName ||
				0 > t.idxGender ||
				0 > t.idxCount ||
				0 > t.idxPercent ||
				0 > t.idxProp100K)

			if bIncompleteSet {
				if 0 > t.idxName {
					err = fmt.Errorf("TSingleDS.LoadFromFile(%s) 'Name' header index has not been parsed", fullfilepath)
					t.Log.Errorln(err.Error())
					return err
				}
				if 0 > t.idxCount {
					t.Log.Warningf("TSingleDS.LoadFromFile(%s) 'Count' header has not been parsed - dataset will be assumed as unranked", fullfilepath)
				}
			} else {
				t.Log.Infof("TSingleDS.LoadFromFile(%s) all expected headers has been parsed(%d items)", fullfilepath, headers_parsed)
			}
			continue
		}

		//now parse all data lines:
		t.Log.Debugf("TSingleDS.LoadFromFile(%s) parsing data line #%d", fullfilepath, nLine)
		nameViews, gender, count, percent, prop100k, isnocount, comment, err := t.loadFromFile_ParseData(line)
		if nil != err {
			t.Log.Errorf("TSingleDS.LoadFromFile(%s) line #%d parsing error [%v]", fullfilepath, nLine, err)
			err = nil
			continue
		}

		//Check rates first(to set Unranked property later)
		if bIsAllRatesEqual {
			if -1 == fAllPercentVal {
				fAllPercentVal = percent
			} else if fAllPercentVal != percent {
				bIsAllRatesEqual = false
			}
			if -1 == fAllProp100kVal {
				fAllProp100kVal = prop100k
			} else if fAllProp100kVal != prop100k {
				bIsAllRatesEqual = false
			}
		}

		t.Log.Debugf("TSingleDS.LoadFromFile(%s) line #%d parsed", fullfilepath, nLine)
		//Create a new data structure
		ni := &TNameFullInfo{
			LineNum:    nLine,
			Commented:  false,
			Name:       nil,
			Gender:     gender,
			Count:      int64(count),
			Percent:    percent,
			Prop100k:   prop100k,
			IsUnranked: isnocount,
			Comment:    comment,
		}

		//Split all names
		nameViewsSplit := strings.Split(nameViews, namesFieldDelimiter)
		t.Log.Debugf("TSingleDS.LoadFromFile(%s) line #%d. Build also latin views", fullfilepath, nLine)
		nameViewsAll := t.appendLatinViews(&nameViewsSplit)
		t.Log.Debugf("TSingleDS.LoadFromFile(%s) line #%d. Adding all name views", fullfilepath, nLine)
		for _, currNameView := range nameViewsAll {
			if slicetools.AddIfNewString(&ni.Name, currNameView) {
				//Add map enty
				t.NameInfoMap[currNameView] = ni
			}
		} //loop by all name views

		t.NameInfoArr = append(t.NameInfoArr, ni)
		nDataLines++
		if 0 == percent {
			nPercent0++
		}
		if 0 == prop100k {
			nProp100k0++
		}

		nCountedTotal += ni.Count
		if cDSContentGenderM == ni.Gender {
			nCountedTotalM += ni.Count
		} else if cDSContentGenderF == ni.Gender {
			nCountedTotalF += ni.Count
		}
	} //loop by file

	err = scanner.Err()
	if err == io.EOF {
		err = nil
		if bWaitTotals {
			err = fmt.Errorf("TSingleDS.LoadFromFile(%s) EOF reached but totals are still expected", fullfilepath)
		} else if bWaitHeaders {
			err = fmt.Errorf("TSingleDS.LoadFromFile(%s) EOF reached but headers are still expected", fullfilepath)
		} else if 0 == nNamesLoaded { //Check file structure error
			err = fmt.Errorf("TSingleDS.LoadFromFile(%s) NO names has been loaded from it", fullfilepath)
		}
	}
	if nil != err {
		t.Log.Errorf(err.Error())
		return err
	}

	t.LinesTotal = nLine + 1

	// if bIsAllRatesEqual {

	// }
	//check and correct totals using counted totals
	if (0 >= t.Total && nCountedTotal > nDataLines) ||
		nProp100k0 == nDataLines ||
		nPercent0 == nDataLines {
		t.Log.Warningf("TSingleDS.LoadFromFile(%s) FileTotal: %d, CountedTotal: %d; DataLines: %d Lines with 0Percent: %d Lines with 0Prop100k: %d Need to re-calculate ranks",
			fullfilepath, t.Total, nDataLines, nCountedTotal, nPercent0, nProp100k0)
		t.recheckAllRanks()
		t.IsRanked = true
		bTotalsNotPresent = false //correct too
		bIsAllRatesEqual = false
		//t.Total, t.TotalM, t.TotalF = nCountedTotal, nCountedTotalM, nCountedTotalF
	}
	// if nPercent0 == nDataLines || nProp100k0 == nDataLines {
	// 	bRecheckRanks = true
	// }

	//Check and fix totals and 'ranked' property:
	if err = t.setRankedPropertyAndFixTotals(bIsAllRatesEqual, bTotalsNotPresent, nCountedTotal, nCountedTotalM, nCountedTotalF); nil != err {
		return err
	}

	return nil
}

func (t *TSingleDS) getFileNameCorrected(basedir string) string {
	if nil == t {
		panic("TSingleDS.getFileNameCorrected called with nil 'this'")
	}
	t.Log.Debugf("TSingleDS.getFileNameCorrected() called")

	dir := basedir
	if 0 == len(dir) {
		dir = filepath.Dir(t.Filename)
	}
	fname := filetools.GetFilename(t.Filename, false)
	ext := filepath.Ext(t.Filename)
	fullname := ""
	if t.IsRanked {
		fname_new := strings.Replace(fname, cFilenamePartIsNoCount, "", -1) + ext
		fullname = filepath.Join(dir, fname_new)
	} else {
		if strings.Contains(strings.ToUpper(fname), cFilenamePartIsNoCount) {
			fullname = t.Filename
		} else {
			fname_new := fname + "_" + cFilenamePartIsNoCount + ext
			fullname = filepath.Join(dir, fname_new)
		}
	}

	return fullname
}

func (t *TSingleDS) SaveToFile(fullfilepath string, deleteOldOneIfNew bool) (err error) {
	if nil == t {
		panic("TSingleDS.SaveToFile called with nil 'this'")
	}
	t.Log.Infof("TSingleDS.SaveToFile(%s) called", fullfilepath)

	if fullfilepath != t.Filename && deleteOldOneIfNew {
		t.Log.Infof("TSingleDS.SaveToFile(%s) failename to save corrected. Remove the oldone file %s", fullfilepath, t.Filename)
		if err := os.Remove(t.Filename); nil != err {
			errNew := fmt.Errorf("TSingleDS.SaveToFile(%s) can't delete original file %s. Error: %v", fullfilepath, t.Filename, err)
			t.Log.Errorf(errNew.Error())
			return errNew
		}
	}

	t.Log.Debugf("TSingleDS.SaveToFile(%s) create/open file to save", fullfilepath)
	file, err := os.Create(fullfilepath)
	if nil != err {
		errNew := fmt.Errorf("TSingleDS.SaveToFile(%s) file create error [%v]", fullfilepath, err)
		t.Log.Errorf(errNew.Error())
		return errNew
	}

	t.Log.Debugf("TSingleDS.SaveToFile(%s) target file created", fullfilepath)
	map_name_lines := make(map[int64]*TNameFullInfo)
	for _, name_info := range t.NameInfoArr {
		map_name_lines[name_info.LineNum] = name_info
	}
	var nLine int64 = -1
	bTotalsAndHeadersSaved := false
	for nLine < t.LinesTotal*2 { // * 2 - just in case
		nLine++
		t.Log.Debugf("TSingleDS.SaveToFile(%s) processing line %d", fullfilepath, nLine)
		if comment_line, ok := t.FileComments[nLine]; ok {
			file.WriteString(comment_line + "\n")
			continue
		}
		//
		if !bTotalsAndHeadersSaved {
			file.WriteString(cDSContentValTotal + ":," + fmt.Sprintf("%d", t.Total) + "\n")
			file.WriteString(cDSContentValTotalM + ":," + fmt.Sprintf("%d", t.TotalM) + "\n")
			file.WriteString(cDSContentValTotalF + ":," + fmt.Sprintf("%d", t.TotalF) + "\n")
			file.WriteString(cTblHdrAll + "\n")
			bTotalsAndHeadersSaved = true
		}

		if nameInfo, ok := map_name_lines[nLine]; ok {
			names_line := t.getLineToSave(nameInfo)
			file.WriteString(names_line + "\n")
		}
	}
	t.Filename = fullfilepath

	t.Log.Infof("TSingleDS.SaveToFile(%s) dataset saved", fullfilepath)
	return nil
}

func (t *TSingleDS) getLineToSave(nameInfo *TNameFullInfo) string {

	retVal := ""
	if nameInfo.Commented {
		retVal = "#"
	}
	for idx, nameView := range nameInfo.Name {
		if idx > 0 {
			retVal += cDSContentNameViewSep
		}
		retVal += nameView
	}
	retVal += ","
	if cDSContentGenderM == nameInfo.Gender {
		retVal += "M"
	} else if cDSContentGenderF == nameInfo.Gender {
		retVal += "F"
	} else if cDSContentGenderU == nameInfo.Gender {
		retVal += "U"
	}

	retVal += fmt.Sprintf(",%d", nameInfo.Count)

	if nameInfo.IsUnranked {
		retVal += fmt.Sprintf(",%."+cDSContentPrecisionNoCountPerc+"f%%", nameInfo.Percent)
		retVal += fmt.Sprintf(",%."+cDSContentPrecisionNoCountProp+"f", nameInfo.Prop100k)
	} else {
		retVal += fmt.Sprintf(",%."+cDSContentPrecisionFloat+"f%%", nameInfo.Percent)
		retVal += fmt.Sprintf(",%."+cDSContentPrecisionFloat+"f", nameInfo.Prop100k)
	}
	if nameInfo.IsUnranked {
		retVal += ",1"
	} else {
		retVal += ",0"
	}
	retVal += "," + nameInfo.Comment
	return retVal
}

func (t *TSingleDS) setRankedPropertyAndFixTotals(bIsAllRatesEqual bool, bTotalsNotPresent bool, nCountedTotal, nCountedTotalM, nCountedTotalF int64) (err error) {
	const tooSmallTotals = 11

	if bTotalsNotPresent || bIsAllRatesEqual {
		t.IsRanked = false
	} else {
		if t.Subset == TDsSubsetLastname {
			t.IsRanked = (tooSmallTotals < t.Total)
		} else {
			t.IsRanked = (tooSmallTotals < t.Total && tooSmallTotals < t.TotalM && tooSmallTotals < t.TotalF)
		}
	}
	//re-check one more time:
	if !t.IsRanked && !bIsAllRatesEqual {
		if t.Total > tooSmallTotals || t.TotalM > tooSmallTotals || t.TotalF > tooSmallTotals {
			t.Log.Warningf("TSingleDS.LoadFromFile(%s) Totals are(T,M,F): %d, %d, %d. Now assumed as ranked", t.Filename, t.Total, t.TotalM, t.TotalF)
			t.IsRanked = true
		}
	}

	//And correct total(s) value(s)
	if t.IsRanked {
		if t.Total <= tooSmallTotals && nCountedTotal > 0 {
			t.Log.Warningf("TSingleDS.LoadFromFile(%s) Total is too small: %d counted value set: %d ", t.Total, nCountedTotal)
			t.Total = nCountedTotal
		}
		if t.TotalM <= tooSmallTotals && nCountedTotalM > 0 {
			t.Log.Warningf("TSingleDS.LoadFromFile(%s) TotalM is too small: %d counted value set: %d ", t.TotalM, nCountedTotalM)
			t.TotalM = nCountedTotalM
		}
		if t.TotalF <= tooSmallTotals && nCountedTotalF > 0 {
			t.Log.Warningf("TSingleDS.LoadFromFile(%s) TotalF is too small: %d counted value set: %d ", t.TotalF, nCountedTotalF)
			t.TotalF = nCountedTotalF
		}
	} else {
		t.Total, t.TotalM, t.TotalF = 1, 1, 1

		for _, nameInfo := range t.NameInfoArr {
			nameInfo.IsUnranked = true
			nameInfo.Count = defUnrankedCount
			nameInfo.Percent = defUnrankedPercent
			nameInfo.Prop100k = defUnrankedProp100k
		}
	}

	return nil
}

func (t *TSingleDS) recheckAllRanks() {
	//
	t.Log.Infof("TSingleDS.recheckAllRanks() file[%s] started. Count totals", t.Filename)

	t.Total, t.TotalM, t.TotalF = 0, 0, 0
	for _, nameInfo := range t.NameInfoArr {
		t.Total += nameInfo.Count
		if cDSContentGenderM == nameInfo.Gender {
			t.TotalM += nameInfo.Count
		} else if cDSContentGenderF == nameInfo.Gender {
			t.TotalF += nameInfo.Count
		}
	} //loop by all name views

	t.Log.Infof("TSingleDS.recheckAllRanks() file[%s]. Totals are: %d; M: %d; F: %d", t.Filename, t.Total, t.TotalM, t.TotalF)
	if 0 >= t.Total {
		t.Log.Errorf("TSingleDS.recheckAllRanks() file[%s] 'Total'==%d - ranks could not be calculated", t.Filename, t.Total)
		return
	}
	for _, nameInfo := range t.NameInfoArr {
		nameInfo.IsUnranked = false
		nameInfo.Percent = float64(nameInfo.Count) * 100 / float64(t.Total)
		nameInfo.Prop100k = 1000 * nameInfo.Percent
	} //loop by all name views
	t.Log.Infof("TSingleDS.recheckAllRanks() file[%s] started. Ranks calculations finished", t.Filename, t.Total, t.TotalM, t.TotalF)
}

func (t *TSingleDS) reconstructNamesMapAndJoinNameInfo() {
	//
	t.Log.Infof("TSingleDS.reconstructNamesMapAndJoinNameInfo(%s) Reconstruct names map by looking for views is already added as another name view", t.Filename)
	t.Log.Infof("TSingleDS.reconstructNamesMapAndJoinNameInfo(%s) NamesInfo array size: %d, map size: %d", t.Filename, len(t.NameInfoArr), len(t.NameInfoMap))
	t.NameInfoMap = make(map[string]*TNameFullInfo)
	total_name_views := 0
	total_name_infos := 0
	for ni_idx, currNameInfo := range t.NameInfoArr {
		t.Log.Debugf("TSingleDS.reconstructNamesMapAndJoinNameInfo() lookup if ANY of name view of NameInfo #%d is already added to a map with their info", ni_idx)
		var names_not_added_yet []string
		already_exists := make(map[string]*TNameFullInfo)
		if 1 < len(currNameInfo.Name) {
			t.Log.Debugf("TSingleDS.reconstructNamesMapAndJoinNameInfo(%s) NamesInfo.Names array size: %d", t.Filename, len(currNameInfo.Name))
		}
		for _, currNameView := range currNameInfo.Name {
			//if pExistCurrent, _ := t.NameInfoMap[currNameView]; nil != pExistCurrent && currNameInfo != pExistCurrent {
			if pExistCurrent, _ := t.NameInfoMap[currNameView]; nil != pExistCurrent {
				t.Log.Debugf("TSingleDS.reconstructNamesMapAndJoinNameInfo() NameInfo #%d view %s already present in a map", ni_idx, currNameView)
				already_exists[currNameView] = pExistCurrent
			} else {
				t.Log.Debugf("TSingleDS.reconstructNamesMapAndJoinNameInfo() NameInfo #%d view %s not present in a map", ni_idx, currNameView)
				//current name view has not been added to map yet
				if slicetools.AddIfNewString(&names_not_added_yet, currNameView) {
					total_name_views++
				}
			}
		}

		if 0 >= len(already_exists) {
			t.Log.Debugf("TSingleDS.reconstructNamesMapAndJoinNameInfo() NameInfo #%d. No any name view already exist in a map - just add all views to map", ni_idx)
			for _, currNameView := range names_not_added_yet { //actually names_not_added_yet contains ALL views in this case
				t.NameInfoMap[currNameView] = currNameInfo
			}
			total_name_infos++
		} else { //In case at least one Info is already added to a map:
			nameInfoToSet, nameViewsOfAlreadyExist := t.constructJoinedNamesInfo(&already_exists)
			var allNameViewsToAddUpdate []string
			slicetools.AppendUniqueString(&allNameViewsToAddUpdate, nameViewsOfAlreadyExist)
			slicetools.AppendUniqueString(&allNameViewsToAddUpdate, names_not_added_yet)
			//Collect final set of all names into an info to update:
			slicetools.AppendUniqueString(&nameInfoToSet.Name, allNameViewsToAddUpdate)
			//and now add/update nameInfo for all present and new nameViews
			// if 2 < len(allNameViewsToAddUpdate) {
			// 	t.Log.Warningln("")
			// }
			for _, nameView := range allNameViewsToAddUpdate {
				t.NameInfoMap[nameView] = nameInfoToSet
			}
		}
	} //loop by all name views

	//recounted
	t.Log.Infof("TSingleDS.reconstructNamesMapAndJoinNameInfo(%s) Re-counted unique name views: %d name infos: %d\nNameInfos map size: %d", t.Filename, total_name_views, total_name_infos, len(t.NameInfoMap))
	//and now reconstruct an array by re-create only pNameInfo from new map created:
	t.Log.Infof("TSingleDS.reconstructNamesMapAndJoinNameInfo(%s) Reconstruct names array", t.Filename)
	t.NameInfoArr = nil
	var testArray []string
	for _, pNameInfo := range t.NameInfoMap {
		testVal := fmt.Sprintf("%v", pNameInfo)
		if slicetools.AddIfNewString(&testArray, testVal) {
			t.NameInfoArr = append(t.NameInfoArr, pNameInfo)
		}
	}
	t.Log.Infof("TSingleDS.reconstructNamesMapAndJoinNameInfo(%s) Names array successfully reconstructed", t.Filename)
}

func (t *TSingleDS) constructJoinedNamesInfo(already_exists *map[string]*TNameFullInfo) (nameInfoToSet *TNameFullInfo, nameViews []string) {
	if nil == already_exists || 0 == len(*already_exists) {
		return nil, nil
	}

	//TODO!!!!!! ADD check each value of count, percent, prop100k and SwitchOFF flag bIsNoCountByVals when loading from file

	//build all nameViews:
	for nameView, pInfo := range *already_exists {
		slicetools.AddIfNewString(&nameViews, nameView)
		slicetools.AppendUniqueString(&nameViews, pInfo.Name)
	}

	//if UNRANKED processing - nothing to check
	if !t.IsRanked {
		for _, pInfo := range *already_exists {
			pInfo.Count = defUnrankedCount
			pInfo.IsUnranked = true
			pInfo.Percent = defUnrankedPercent
			pInfo.Prop100k = defUnrankedProp100k
			return pInfo, nameViews
		}
	}
	//the only info exist
	if 1 == len(*already_exists) {
		for _, pInfo := range *already_exists {
			return pInfo, nameViews
		}
	}

	//some ranks are present
	var pRetInfo *TNameFullInfo
	bIsDoubledSameName := true
	bIsAllCount1 := true
	var valPrevCount, sumCount int64
	var valPrevPercent, sumPercent, valPrevProp100k, sumProp100k float64
	for _, pInfo := range *already_exists {
		if nil == pRetInfo {
			pRetInfo = pInfo
			valPrevCount, sumCount = pInfo.Count, pInfo.Count
			valPrevPercent, sumPercent = pInfo.Percent, pInfo.Percent
			valPrevProp100k, sumProp100k = pInfo.Prop100k, pInfo.Prop100k
			continue
		}
		sumCount += pInfo.Count
		sumPercent += pInfo.Percent
		sumProp100k += pInfo.Prop100k

		if bIsDoubledSameName {
			if valPrevCount != pInfo.Count {
				bIsDoubledSameName = false
			} else if valPrevPercent != pInfo.Percent {
				bIsDoubledSameName = false
			} else if valPrevProp100k != pInfo.Prop100k {
				bIsDoubledSameName = false
			}
		}

		if 1 != pInfo.Count {
			bIsAllCount1 = false
		}
	}

	if bIsAllCount1 && t.IsRanked {
		pRetInfo.Count = sumCount
		pRetInfo.IsUnranked = false
		pRetInfo.Percent = sumPercent
		pRetInfo.Prop100k = sumProp100k
		return pRetInfo, nameViews
	}

	if bIsDoubledSameName {
		return pRetInfo, nameViews
	}

	pRetInfo.Count = sumCount
	pRetInfo.IsUnranked = false
	pRetInfo.Percent = sumPercent
	pRetInfo.Prop100k = sumProp100k
	return pRetInfo, nameViews
}

func (t *TSingleDS) appendLatinViews(nameViewsSplit *[]string) []string {
	nameViewsMap := make(map[string]bool)
	for _, nameView := range *nameViewsSplit {
		currNameView := strings.Trim(nameView, " \t\n"+cDSContentNameViewSep)
		if 0 == len(currNameView) {
			continue
		}
		nameViewsMap[currNameView] = true

		latinNameView := stringtools.GetLatinView(currNameView, latinViewAlsoAllowedSet)
		if 0 < len(latinNameView) && latinNameView != currNameView {
			nameViewsMap[latinNameView] = true
		}
	} //loop by name views

	var nameViewsAll []string
	for nameView, _ := range nameViewsMap {
		nameViewsAll = append(nameViewsAll, nameView)
	} //loop by all undoubled name views
	return nameViewsAll
}

func (t *TSingleDS) addUpdateNameViewInfoVariants(fullfilepath string, nLine int, nameview string, propval float64, dataset *map[string]*TNameInfo, pNamesLoaded *int64) {

	t.addUpdateNameViewInfo(fullfilepath, nLine, nameview, propval, dataset, pNamesLoaded)
	latinNameView := stringtools.GetLatinView(nameview, latinViewAlsoAllowedSet)
	if 0 < len(latinNameView) && latinNameView != nameview {
		t.Log.Debugf("TColDetect.addUpdateNameViewInfoVariants(...) File %s line %d name view %s has also latin-only view %s - try to add it too",
			fullfilepath, nLine, nameview, latinNameView)
		t.addUpdateNameViewInfo(fullfilepath, nLine, latinNameView, propval, dataset, pNamesLoaded)
	}
}

func (t *TSingleDS) addUpdateNameViewInfo(fullfilepath string, nLine int, nameview string, propval float64, dataset *map[string]*TNameInfo, pNamesLoaded *int64) {

	existVal, isExist := (*dataset)[nameview]
	if !isExist {
		ni := &TNameInfo{
			Name:     make([]string, 1),
			Prop100k: propval,
		}
		ni.Name[0] = nameview

		(*pNamesLoaded)++
		(*dataset)[nameview] = ni
		t.Log.Debugf("TColDetect.addUpdateNameViewInfo(...) File %s line %d new name view %s added",
			fullfilepath, nLine, nameview)
	} else { //already exist
		if propval > existVal.Prop100k {
			existVal.Prop100k = propval
			t.Log.Debugf("TColDetect.addUpdateNameViewInfo(...) File %s line %d name view %s already loaded, coeff updated %f->%f",
				fullfilepath, nLine,
				nameview, existVal.Prop100k, propval)
		} else {
			t.Log.Debugf("TColDetect.addUpdateNameViewInfo(...) File %s line %d name view %s already loaded with the same or bigger coeff", fullfilepath, nLine, nameview)
		}
	}
}

func (t *TSingleDS) loadFromFile_ParseTotals(line string) bool {
	if nil == t {
		panic("TSingleDS.loadFromFile_ParseTotals called with nil 'this'")
	}
	something_parsed := false
	csvParts := strings.Split(line, cTblDelimiter)
	if 0 >= len(csvParts) {
		t.Log.Warningf("Wait for 'Totals' line but empty line got [%s]", line)
	}

	var err error
	if 2 <= len(csvParts) {
		// //TOTALS:
		// cDSContentValTotal    = "TOTAL"
		// cDSContentValTotalM   = "TOTALM"
		// cDSContentValTotalF   = "TOTALF"
		//Old one constants
		// Total births:,1658699,,,
		// Total M:,895801,,,
		// Total F:,762898,,,
		totalName := strings.Trim(csvParts[0], cTrimTotals)
		totalStr := strings.Trim(csvParts[1], cTrimTotals)
		var totalVal int
		totalVal, err = strconv.Atoi(totalStr)
		if cDSContentValTotalM == totalName || cDSContentValTotalM_Old == totalName {
			t.TotalM = int64(totalVal)
			something_parsed = true
		} else if cDSContentValTotalF == totalName || cDSContentValTotalF_Old == totalName {
			t.TotalF = int64(totalVal)
			something_parsed = true
		} else if cDSContentValTotal == totalName || cDSContentValTotal_Old == totalName {
			t.Total = int64(totalVal)
			something_parsed = true
		} else {
			t.Log.Warningf("Unrecognized 'Totals' line [%s]", line)
		}

		if nil != err {
			t.Log.Warningf("Error [%s] parsing 'Totals' line [%s]", err.Error(), line)
			err = nil
		}
	} else {
		t.Log.Warningf("'Totals' line expected. It must contain at least 2 fields [%s]", line)
	}
	return something_parsed
}

func (t *TSingleDS) loadFromFile_ParseHeaders(line string) (nHrdRecognized int) {
	if nil == t {
		panic("TSingleDS.loadFromFile_ParseHeaders called with nil 'this'")
	}
	csvParts := strings.Split(line, cTblDelimiter)
	if 0 >= len(csvParts) {
		t.Log.Warningf("Headers line expected but empty line got [%s]", line)
		return
	}

	if cTblMinFieldsCount <= len(csvParts) {
		// cTblHdrName          = "NAME"
		// cTblHdrGender        = "GENDER"
		// cTblHdrCount         = "COUNT"
		// cTblHdrPercent       = "PERCENT"
		// cTblHdrProp100k      = "PROP100K"
		// cTblHdrIsNoRealCount = "ISNOCOUNT"
		// cTblHdrComment       = "COMMENT"
		for idx, val := range csvParts {
			switch val {
			case cTblHdrName:
				nHrdRecognized++
				t.idxName = idx
			case cTblHdrGender:
				nHrdRecognized++
				t.idxGender = idx
			case cTblHdrCount:
				nHrdRecognized++
				t.idxCount = idx
			case cTblHdrPercent:
				nHrdRecognized++
				t.idxPercent = idx
			case cTblHdrProp100k:
				nHrdRecognized++
				t.idxProp100K = idx
			case cTblHdrIsNoRealCount:
				nHrdRecognized++
				t.idxIsFakeRank = idx
			case cTblHdrIsNoRealCount2:
				nHrdRecognized++
				t.idxIsFakeRank = idx
			case cTblHdrComment:
				nHrdRecognized++
				t.idxComment = idx
			default:
				t.Log.Errorf("Unknown header name reached [%s]", val)
			}
		}

		if nHrdRecognized != len(csvParts) {
			t.Log.Warningf("Headers line parsed %d of %d fields", nHrdRecognized, len(csvParts))
		}
	} else {
		t.Log.Warningf("Headers line expected. It must contain at least %d fields", cTblMinFieldsCount)
		t.Log.Warningf(" Got line [%s] instead. Parts: %v", line, csvParts)
	}
	return
}

func (t *TSingleDS) loadFromFile_ParseData(line string) (nameViews string, gender rune, count int, percent float64, prop100k float64, isnocount bool, comment string, err error) {
	if nil == t {
		panic("TSingleDS.loadFromFile_ParseData called with nil 'this'")
	}

	csvParts := strings.Split(line, cTblDelimiter)
	if 0 >= len(csvParts) {
		t.Log.Warningf("Wait for 'data' line but empty line got [%s]", line)
		return
	}

	if t.idxName >= 0 {
		if t.idxName < len(csvParts) {
			nameViews = csvParts[t.idxName]
		} else {
			err = fmt.Errorf("data line got [%s] has less fields than 'name' index [%d]", line, t.idxName)
		}
	} else {
		err = fmt.Errorf("data line got [%s] but 'name' index is invalid [%d]", line, t.idxName)
	}
	if nil != err {
		return
	}

	if t.idxGender >= 0 {
		if t.idxGender < len(csvParts) && 0 < len(csvParts[t.idxGender]) {
			gender = []rune(csvParts[t.idxGender])[0]
		}
	}

	isnocount = true
	if t.idxCount >= 0 {
		if t.idxCount < len(csvParts) {
			count, err = strconv.Atoi(csvParts[t.idxCount])
		} else {
			err = fmt.Errorf("data line got [%s] has less fields than 'count' index [%d]", line, t.idxCount)
		}
	} else {
		err = fmt.Errorf("data line got [%s] but 'count' index is [%d]", line, t.idxCount)
	}
	if nil != err {
		return
	}

	if t.idxProp100K >= 0 {
		if t.idxProp100K < len(csvParts) {
			prop100k, err = strconv.ParseFloat(csvParts[t.idxProp100K], 64)
		} else {
			err = fmt.Errorf("data line got [%s] has less fields than 'Prop100K' index [%d]", line, t.idxProp100K)
		}
	} else {
		err = fmt.Errorf("data line got [%s] but 'Prop100K' index is [%d]", line, t.idxProp100K)
	}
	if nil != err {
		return
	}

	if t.idxPercent >= 0 {
		if t.idxPercent < len(csvParts) {
			percent_val_str := strings.Trim(csvParts[t.idxPercent], "%")
			percent, err = strconv.ParseFloat(percent_val_str, 64)
		} else {
			err = fmt.Errorf("data line got [%s] has less fields than 'percent' index [%d]", line, t.idxPercent)
		}
	} else {
		//err = fmt.Errorf("data line got [%s] but 'percent' index is [%d]", line, t.idxPercent)
		t.Log.Warningf("data line got [%s] but 'percent' index is [%d]", line, t.idxPercent)
	}
	if nil != err {
		return
	}

	isnocount = false
	if t.idxIsFakeRank >= 0 {
		if t.idxIsFakeRank < len(csvParts) {
			intVal := 0
			intVal, err = strconv.Atoi(csvParts[t.idxIsFakeRank])
			if nil != err {
				return
			}
			isnocount = (0 != intVal)
		}
	}

	if t.idxComment >= 0 {
		if t.idxComment < len(csvParts) {
			comment = csvParts[t.idxComment]
		}
	}
	return
}

func (t *TCountryNameDS) ImportDataset(country string, subset TDsSubset, filename string) error {
	if nil == t {
		panic("TCountryNameDS.ImportDataset called with nil 'this'")
	}

	return nil
}

func (t *TSingleDS) loadSubDataSet_prop100k(dirpath string, dataset *map[string]*TNameInfo) error {
	subfiles, _ := ioutil.ReadDir(dirpath)
	for _, subf := range subfiles {
		if !subf.IsDir() {
			fullfilepath := filepath.Join(dirpath, subf.Name())
			//file, err := os.OpenFile(fullfiTColDetectlepath, os.O_RDONLY, 0666)
			file, err := os.Open(fullfilepath)
			if nil != err {
				return err
			}
			scanner := bufio.NewScanner(bufio.NewReader(file))
			nLine := -1
			idxFldProp100k := -1
			idxFldName := -1
			nNamesLoaded := 0
			for scanner.Scan() {
				nLine++
				line := strings.ToUpper(strings.Trim(scanner.Text(), "\n\r\t "))

				if 0 == len(line) {
					continue
				}

				if -1 == idxFldProp100k { //check headers first:
					if idxFldProp100k = t.getFieldIndex(line, tblFieldNameProp100k, tblSep); -1 == idxFldProp100k {
						//return fmt.Errorf("Error! File %s doesn't contain header's field %s", fullfilepath, tblFieldNameProp100k)
						continue
					}
					if idxFldName = t.getFieldIndex(line, tblFieldNameName, tblSep); -1 == idxFldName {
						//return fmt.Errorf("Error! File %s doesn't contain header's field %s", fullfilepath, tblFieldNameName)
						continue
					}
					continue
				}

				name, probab, err := t.getNameAndProbability(line, idxFldName, idxFldProp100k, tblSep)
				if nil != err {
					return err
				}

				nameViews := strings.Split(name, namesFieldDelimiter)

				for _, nameView := range nameViews {
					currNameView := strings.Trim(nameView, " \t\n")
					if 0 == len(currNameView) {
						continue
					}

					ni := &TNameInfo{
						Name:     make([]string, 1),
						Prop100k: probab,
					}
					ni.Name[0] = name

					existVal, isExist := (*dataset)[name]
					if !isExist {
						nNamesLoaded++
						(*dataset)[name] = ni
					} else { //already exist
						if ni.Prop100k > existVal.Prop100k {
							existVal.Prop100k = ni.Prop100k
						}
					}
				} //loop by name views
			} //loop by file

			err = scanner.Err()
			if nil != err {
				return err
			}

			if 0 == nNamesLoaded { //Check file structure error
				return fmt.Errorf("Check structure of file %s. NO names has been loaded from it!", fullfilepath)
			}

		}
	}
	return nil
}

func (t *TSingleDS) getFieldIndex(headerline string, fieldname string, sep string) int {
	if !strings.Contains(headerline, fieldname) {
		return -1
	}
	//get the index of field in a header
	headers := strings.Split(headerline, sep)
	for n, headerName := range headers {
		if 0 == strings.Compare(headerName, fieldname) {
			return n
		}
	}

	return -1
}

func (t *TSingleDS) getNameAndProbability(line string, nameIdx int, probabIdx int, sep string) (string, float64, error) {
	//TODO!!! add NAME field split for sinonyms if present and return []string in this case
	if 0 > nameIdx || 0 > probabIdx {
		return "", 0, errors.New("Invalid field index < 0")
	}

	fields := strings.Split(line, sep)
	if nameIdx >= len(fields) || probabIdx >= len(fields) {
		return "", 0, errors.New("Invalid field index >= count of fields")
	}

	probab, err := strconv.ParseFloat(fields[probabIdx], 64)
	if nil != err {
		return "", 0, err
	}
	return fields[nameIdx], probab, nil
}

func (t *TCountryNameDS) createLogfile() string {
	fnameWorkLog := filepath.Join(t.DSFullPath)
	//TODO
	return fnameWorkLog
}
