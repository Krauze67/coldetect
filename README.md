# Country Of Living Detector

## Description

coldetect() is golang package used to detect person's country of living by different full name.

## Use

### Basic Use

```go
package main
import "gitlab.com/Krauze67/coldetect"

func main() {
    //One-time initialization
    detector := coldetect.TColDetect{}
    countriesDatasetsPath := "/home/user1/ds"
    detector.Init(countriesDatasetsPath)


    //Get a list of all countries which has some dataset to detect
    countryList, err := detector.GetCountryList()
    fmt.Println("Countries has datasets:")
    for _, countyCode2Symb := range countryList {
        fmt.Printf("\t%s,\n", countyCode2Symb)
    }

    //Detect most propable country by fullname
    countryCode, score, err := detector.GetMostProbableCountryByFullname("Dr. John William Smith Jr III")
    fmt.Printf("Country code: %s\n", countryCode)
    fmt.Printf("Detected score: %f\n", score)

    //Get ISO3166 info of country by it's Alpha2 code
    countryInfoMap := detector.GetISO3166Info(countryCode)
    fmt.Println("Country info(ISO3166):")
    for field, val := range countryInfoMap {
        fmt.Printf("\t%s: %f\n", field, val)
    }

    //Get a list of countries and it's detect scores by fullname:
    countriesDetected, err := detector.GetCountriesByFullname("Dr. John William Smith Jr III")
    fmt.Println("Fullname detect.\nCountries and their scores(in descending order by frequency for 100k of population):")
    for i, detectRes := range countriesDetected {
        fmt.Printf("\t%d. Country code:%s, score: %f\n", i, detectRes.Country, detectRes.Score)
    }

    //Get a list of countries and it's detect scores by firstname:
    countriesDetected, err = detector.GetCountriesByFirstname("John")
    fmt.Println("Firstname detect.\nCountries and their scores(in descending order by frequency for 100k of population):")
    for i, detectRes := range countriesDetected {
        fmt.Printf("\t%d. Country code:%s, score: %f\n", i, detectRes.Country, detectRes.Score)
    }

    //Get a list of countries and it's detect scores by last:
    countriesDetected, err = detector.GetCountriesByLastname("Smith")
    fmt.Println("Lastname detect.\nCountries and their scores(in descending order by frequency for 100k of population):")
    for i, detectRes := range countriesDetected {
        fmt.Printf("\t%d. Country code:%s, score: %f\n", i, detectRes.Country, detectRes.Score)
    }

    //Get a list of countries and it's detect scores by middle name(s):
    middlenames := []string{"Huan", "G.", "Maria", "Carlos", "Ferdinand", "Gonsalvo", "Isabella"}
    countriesDetected, err = detector.GetCountriesByMiddlenames(middlenames)
    fmt.Println("Middlename(s) detect.\nCountries and their scores(in descending order by frequency for 100k of population):")
    for i, detectRes := range countriesDetected {
        fmt.Printf("\t%d. Country code:%s, score: %f\n", i, detectRes.Country, detectRes.Score)
    }

}
```

### TDetectRes struct

```go
type TDetectRes struct {
    Country string
    Score   float64
}
```

### Datasets structure

	Directories structure must be:
	<DatasetsRootDir>
		<CountryNameAlpha2 as ISO3166 form>
			firstname
				file1.csv
				...
				fileN.csv
			middlename
				file1.csv
				...
				fileN.csv
			lastname
				file1.csv
				...
				fileN.csv
		allcountriesISO3166.csv

#### allcountriesISO3166.csv format
It is tab-divided csv file containing any fields. 
Field "alpha2" is obligatory

#### Dataset's files fileN.csv format details
1. required fields names: NAME, PROP100K. Surname files must also name it 'NAME'
2. all headers and names are case insensitive
3. column PROP100K should contain a value of "approximate count of names(surnames) occurences by 100k of population". It could be calculated using frequency of item (percentage) multiplied to 1000


### Datasets management
To add a new dataset of given names/middle names/surnames call:
```go
	dsroot := "/home/user1/ds"
	country := "US"
	subset := coldetect.TDsSubsetFirstname //or coldetect.TDsSubsetLastname, coldetect.TDsSubsetMidname
	externalLogLevel := log.InfoLevel
	fileLogLevel := log.InfoLevel
	err := coldetect.FixAndRejoinDatasest(dsroot, country, subset, externalLogLevel, fileLogLevel)
```

Function corrects existent datasets by joining different views of the same names and commenting name lines has less rank than in another one file(s) present.
```go
	err := coldetect.FixAndRejoinDatasest(dsroot, country, subset, externalLogLevel, fileLogLevel)
```

Both methods join different name views while processing and as result of work also leave a log of operation and complete backup of previous data:
	<DatasetsRootDir>
		<CountryNameAlpha2 as ISO3166 form>
			[firstname | middlename | lastname]
				_Log_[DATE]-T[TIME].log
				BCKP_[DATE]-T[TIME]
				

Acceptable data formats are:
	1. coplete counted ranked dataset with totals(see above)
	2. csv file has columns NAME and COUNT and without another columns. In case count of names are different all required info will be re-counted and saved in a correct format
	3. csv file has columns NAME. In this case such dataset is assumed as unranked.


