package coldetect

import (
	"errors"
	"sort"
	"strings"
	"sync"
)

const (
	//fullnameSep = " .,\t"
	fullnameSep                  = " "
	nameTrim                     = "- 	.,\t'`"
	cCutResuiltsMinCountryWeigth = 0.1
	cCutResuiltsMinScore         = 0

	MinNameLengthAllowed  = 3
	WeightFirstNameGlobal = 1 //TODO - just for tests
)

// TColDetect all required fields
type TColDetect struct {
	Countries        []*TCountryInfo
	CountriesISO3166 map[string]*TCountryInfoISO3166
	mLockCountries   sync.RWMutex
	// =============
	bInitialized bool
	mLockInit    sync.Mutex
	// =============
}

// TDetectRes a final result of tests
type TDetectRes struct {
	Country string
	Score   float64
}

// TDetectResInt a final result of tests
type TDetectResInt struct {
	Country *TCountryInfo
	Score   float64
}

// TCountryTestRes particular country testing result
type TCountryTestRes struct {
	Country  *TCountryInfo
	FNameVal float64
	LNameVal float64
	MNames   []float64
}

// ExtLogger external logger pointer
//var ExtLogger *Logger

// Init initializes all data
func (t *TColDetect) Init(datasetdir string) error {
	//ExtLogger.Println("TColDetect.Init()")
	if nil == t {
		//ExtLogger.Println("TColDetect.Init() - null pointer")
		return errors.New("Init method called with nil 'this' pointer")
	}
	t.mLockInit.Lock()
	if t.bInitialized {
		//ExtLogger.Println("TColDetect.Init() - already initialized")
		t.mLockInit.Unlock()
		return nil
	}

	t.mLockCountries.Lock()
	t.CountriesISO3166 = make(map[string]*TCountryInfoISO3166)
	err := t.loadDataSets(datasetdir)
	t.mLockCountries.Unlock()

	t.bInitialized = true
	t.mLockInit.Unlock()
	return err
}

// Deinit uninit
func (t *TColDetect) Deinit() error {
	if nil == t {
		return errors.New("Deinit method called with nil 'this' pointer")
	}
	t.mLockInit.Lock()
	defer t.mLockInit.Unlock()
	if !t.bInitialized {
		return nil
	}

	//TODO: 2

	return nil
}

// IsInitialized returns true if initialized
func (t *TColDetect) IsInitialized() bool {
	if nil == t {
		return false
	}
	t.mLockInit.Lock()
	defer t.mLockInit.Unlock()
	return t.bInitialized
}

// GetCountryList returns all countries loaded
func (t *TColDetect) GetCountryList() ([]string, error) {
	if !t.IsInitialized() {
		return nil, errors.New("Initialize first")
	}
	var retVal []string
	t.mLockCountries.RLock()
	defer t.mLockCountries.RUnlock()
	for _, ci := range t.Countries {
		retVal = append(retVal, ci.CountryName)
	}

	return retVal, nil
}

// GetMostProbableCountryByFullname returns all countries matched the full name in descending order of Score
func (t *TColDetect) GetMostProbableCountryByFullname(fullname string) (string, float64, error) {
	detectSet, err := t.GetCountriesByFullname(fullname)
	if nil != err || 0 == len(detectSet) {
		return "", 0, err
	}

	return detectSet[0].Country, detectSet[0].Score, nil
}

// GetCountriesByFullname returns all countries matched the full name in descending order of Score
func (t *TColDetect) GetCountriesByFullname(fullname string) ([]TDetectRes, error) {
	if !t.IsInitialized() {
		return nil, errors.New("Initialize first")
	}

	// 1
	fullnameUP, err := t.doPreprocess(fullname)
	if nil != err {
		return nil, err
	}

	// 2
	fname, lname, midnames := t.doSplit(fullnameUP)

	// 3
	t.doPostprocess(&fname, &lname, &midnames)

	// 4 actually calculate coefficients:
	t.mLockCountries.RLock()
	var testVal []TCountryTestRes
	for _, pInfo := range t.Countries {
		countryRes, err := t.calcCountryCoeff(fname, lname, midnames, pInfo)
		countryRes.Country = pInfo
		if nil == err {
			//TODO: 1 possibly break ongoing checks in case some very good result has been got
			testVal = append(testVal, countryRes)
		}
	}
	t.mLockCountries.RUnlock()

	// 5 analyze results got
	detectResInt, err := t.buildDetectResultsInt(testVal)
	retVal := t.getDetectResultsFromInt(&detectResInt)
	return retVal, err
}

// GetCountriesByFirstname returns all countries matched the full name in descending order of Score
func (t *TColDetect) GetCountriesByFirstname(firstname string) ([]TDetectRes, error) {
	if !t.IsInitialized() {
		return nil, errors.New("Initialize first")
	}

	firstnameToCheck := strings.ToUpper(strings.Trim(firstname, nameTrim))

	var emptyres []TDetectRes
	if MinNameLengthAllowed > len(firstnameToCheck) {
		return emptyres, nil
	}

	// actually calculate coefficients:
	t.mLockCountries.RLock()
	var testVal []TCountryTestRes
	for _, pInfo := range t.Countries {
		countryRes := TCountryTestRes{Country: pInfo}
		countryRes.FNameVal = t.calcCountryCoeff_firstname(firstnameToCheck, pInfo)
		testVal = append(testVal, countryRes)
	}
	t.mLockCountries.RUnlock()

	// sort results got
	detectResInt, err := t.buildDetectResultsInt(testVal)
	retVal := t.getDetectResultsFromInt(&detectResInt)
	return retVal, err
}

// GetCountriesByLastname returns all countries matched the full name in descending order of Score
func (t *TColDetect) GetCountriesByLastname(lastname string) ([]TDetectRes, error) {
	if !t.IsInitialized() {
		return nil, errors.New("Initialize first")
	}

	lastnameToCheck := strings.ToUpper(strings.Trim(lastname, nameTrim))

	var emptyres []TDetectRes
	if MinNameLengthAllowed > len(lastnameToCheck) {
		return emptyres, nil
	}

	// actually calculate coefficients:
	t.mLockCountries.RLock()
	var testVal []TCountryTestRes
	for _, pInfo := range t.Countries {
		countryRes := TCountryTestRes{Country: pInfo}
		countryRes.LNameVal = t.calcCountryCoeff_lastname(lastnameToCheck, pInfo)
		testVal = append(testVal, countryRes)
	}
	t.mLockCountries.RUnlock()

	// sort results got
	detectResInt, err := t.buildDetectResultsInt(testVal)
	retVal := t.getDetectResultsFromInt(&detectResInt)
	return retVal, err
}

// GetCountriesByMiddlenames returns all countries matched the full name in descending order of Score
func (t *TColDetect) GetCountriesByMiddlenames(midnames []string) ([]TDetectRes, error) {
	if !t.IsInitialized() {
		return nil, errors.New("Initialize first")
	}

	var midnamesToCheck []string
	//midnamesToCheck := strings.ToUpper(strings.Trim(lastname, nameTrim))
	for _, mname := range midnames {
		testname := strings.ToUpper(strings.Trim(mname, nameTrim))
		if MinNameLengthAllowed <= len(testname) {
			midnamesToCheck = append(midnamesToCheck, testname)
		}
	}

	//
	var emptyres []TDetectRes
	if 0 >= len(midnamesToCheck) {
		return emptyres, nil
	}

	// actually calculate coefficients:
	t.mLockCountries.RLock()
	var testVal []TCountryTestRes
	for _, pInfo := range t.Countries {
		countryRes := TCountryTestRes{Country: pInfo}
		countryRes.MNames = t.calcCountryCoeff_midname(&midnamesToCheck, pInfo)
		testVal = append(testVal, countryRes)
	}
	t.mLockCountries.RUnlock()

	// sort results got
	detectResInt, err := t.buildDetectResultsInt(testVal)
	retVal := t.getDetectResultsFromInt(&detectResInt)
	return retVal, err
}

func (t *TColDetect) doPreprocess(fullname string) (string, error) {
	if !t.IsInitialized() {
		return "", errors.New("Initialize first")
	}

	return strings.ToUpper(fullname), nil
}

func (t *TColDetect) doPostprocess(pfirst *string, plast *string, pmiddle *[]string) error {
	if !t.IsInitialized() {
		return errors.New("Initialize first")
	}

	return nil
}

func (t *TColDetect) doSplit(fullname string) (first string, last string, middle []string) {
	allnames := strings.Split(fullname, fullnameSep)
	//TODO!!!  re-join all surname parts like
	// possibly inside selectProperNames(...)
	//"De "  (french)
	//"La "  (french)
	// part1 "dit" part2  - full surname
	//...  some of spain, italian
	// leading "'" in Arab
	//TODO - check what else
	allnames = t.removeInvalidNames(allnames)

	return t.selectProperNames(allnames)
}

func (t *TColDetect) selectProperNames(names []string) (first string, last string, middle []string) {
	if nil == names || 0 >= len(names) {
		return "", "", nil
	}

	if 1 == len(names) {
		return "", names[0], nil
	}

	if 2 == len(names) {
		//TODO detect country(language)-specific first-last names order!
		return names[0], names[1], nil
	}

	// 3 >= len(names)
	//TODO detect country(language)-specific first-last names order!
	return names[0], names[len(names)-1], names[1 : len(names)-1]
}

func (t *TColDetect) removeInvalidNames(names []string) []string {
	var retVal []string
	for _, name := range names {
		testname := strings.Trim(name, nameTrim)
		if 1 >= len(testname) {
			continue
		}
		if t.isInvalidName(testname) {
			continue
		}

		retVal = append(retVal, testname)
	}
	return retVal
}

// func (t *TColDetect) isValidFirstName(name string) (bool, error) {
// 	if nil == t {
// 		return false, errors.New("isValidFirstName method called with nil 'this' pointer")
// 	}
// 	if 1 >= len(name) {
// 		return false, nil
// 	}
// 	if res, err := t.isInvalidName(name); res {
// 		return false, err
// 	}

// 	return true, nil
// }

// func (t *TColDetect) isValidLastName(name string) (bool, error) {
// 	if nil == t {
// 		return false, errors.New("isValidFirstName method called with nil 'this' pointer")
// 	}
// 	if 1 >= len(name) {
// 		return false, nil
// 	}
// 	if res, err := t.isInvalidName(name); res {
// 		return false, err
// 	}

// 	return true, nil
// }

func (t *TColDetect) isInvalidName(name string) bool {
	for _, inv := range NotNames {
		if 0 == strings.Compare(name, inv) {
			return true
		}
	}
	return false
}

func (t *TColDetect) calcCountryCoeff_firstname(fname string, pInfo *TCountryInfo) float64 {
	var ret float64
	if nameInfo, ok := pInfo.FirstNames[fname]; ok {
		ret = nameInfo.Prop100k * pInfo.CountrySpecialInfo.CountryWeight *
			pInfo.CountrySpecialInfo.CoeffWeightName

		if 0 < WeightFirstNameGlobal {
			ret *= WeightFirstNameGlobal
		}
	}
	return ret
}

func (t *TColDetect) calcCountryCoeff_lastname(lname string, pInfo *TCountryInfo) float64 {
	var ret float64
	if nameInfo, ok := pInfo.LastNames[lname]; ok {
		ret = nameInfo.Prop100k * pInfo.CountrySpecialInfo.CountryWeight *
			pInfo.CountrySpecialInfo.CoeffWeightSurname

	}
	return ret
}

func (t *TColDetect) calcCountryCoeff_midname(midnames *[]string, pInfo *TCountryInfo) []float64 {
	var ret []float64
	for _, mname := range *midnames {
		if nameInfo, ok := pInfo.MiddleNames[mname]; ok {
			ret = append(ret, nameInfo.Prop100k*pInfo.CountrySpecialInfo.CountryWeight*
				pInfo.CountrySpecialInfo.CoeffWeightMidname)
		}
	}
	return ret
}

func (t *TColDetect) calcCountryCoeff(fname, lname string, midnames []string, pInfo *TCountryInfo) (TCountryTestRes, error) {
	ret := TCountryTestRes{}
	ret.FNameVal = t.calcCountryCoeff_firstname(fname, pInfo)
	ret.LNameVal = t.calcCountryCoeff_lastname(lname, pInfo)
	ret.MNames = t.calcCountryCoeff_midname(&midnames, pInfo)

	return ret, nil
}

func (t *TColDetect) buildDetectResultsInt(countryRes []TCountryTestRes) ([]TDetectResInt, error) {
	var ret []TDetectResInt

	// construct a final scores slice
	for _, testRes := range countryRes {
		detectRes := TDetectResInt{Country: testRes.Country}

		var avgMidnameScore float64
		if 0 < len(testRes.MNames) {
			var midSumVal float64
			for _, mVal := range testRes.MNames {
				midSumVal += mVal
			}
			avgMidnameScore = midSumVal / float64(len(testRes.MNames))
		}

		// ====================== Decision make part =====================
		//TODO: correct weights for max precision
		var (
			decisionWeigthFirstName float64 = 1 //8
			decisionWeigthLastName  float64 = 1 //2
			decisionWeigthMidname   float64 = 1 //1
			decisionDiv             float64 = 3
		)
		// if 0 >= testRes.FNameVal {
		// 	decisionWeigthLastName = 8
		// 	decisionWeigthMidname = 4
		// 	decisionDiv = 2
		// }
		if 0 == len(testRes.MNames) || 0 == avgMidnameScore {
			decisionWeigthMidname = 0
			decisionDiv = 2
		}

		detectRes.Score = (testRes.FNameVal*decisionWeigthFirstName +
			testRes.LNameVal*decisionWeigthLastName +
			avgMidnameScore*decisionWeigthMidname) / decisionDiv

		ret = append(ret, detectRes)
	}

	//Sort scores in descending order(simplest bubblesort)
	sort.Sort(TResSortByScoreDesc(ret))

	//cut all results by first 0 result of Country with weigth==1
	nLastIdx := 0
	for ; nLastIdx < len(ret); nLastIdx++ {
		if ret[nLastIdx].Score <= cCutResuiltsMinScore && ret[nLastIdx].Country.CountrySpecialInfo.CountryWeight <= cCutResuiltsMinCountryWeigth {
			break
		}
	}
	return ret[:nLastIdx], nil
}

func (t *TColDetect) getDetectResultsFromInt(detectInt *[]TDetectResInt) []TDetectRes {
	var ret []TDetectRes

	// construct a final scores slice
	for _, resInt := range *detectInt {
		detectRes := TDetectRes{
			Country: resInt.Country.CountryName,
			Score:   resInt.Score,
		}
		ret = append(ret, detectRes)
	}
	return ret
}
