package coldetect

// NotNames is slice of possible "not name" parts
var NotNames = [...]string{
	"MS",
	"MR",
	"MRS",
	"DR",
	"JR",
	"PROF",
	"II",
	"III",
	"MC",
	"AD",
	"MISS",
	"THE",
	//"EP", //FR specific CAYZAC-EP.-BAS
	//"DE", //FR specific COUSIN-DE-MAUVAISIN
	//"DA", //FR specific DA-GAMA
	//"DAL", //FR specific DAL'FARRA
	//"LA", //FR specific //DE-LA-FERRIERE	250
}
