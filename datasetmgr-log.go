package coldetect

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

// TDsMgrLog
type TDsMgrLog struct {
	//Timestamp   string
	//FileName    string
	File            *os.File
	FileLogLevel    log.Level
	ExtLogLevel     log.Level
	CurrLineNum     int64
	CurrFileLineNum int64
}

func (t *TDsMgrLog) Panic(args ...interface{}) {
	t.Panicln(args...)
}

func (t *TDsMgrLog) Fatal(args ...interface{}) {
	t.Fatalln(args...)
}

func (t *TDsMgrLog) Error(args ...interface{}) {
	t.Errorln(args...)
}

func (t *TDsMgrLog) Warning(args ...interface{}) {
	t.Warningln(args...)
}

func (t *TDsMgrLog) Info(args ...interface{}) {
	t.Infoln(args...)
}

func (t *TDsMgrLog) Debug(args ...interface{}) {
	t.Debugln(args...)
}

func (t *TDsMgrLog) Logln(level log.Level, args ...interface{}) {
	switch level {
	case log.PanicLevel:
		t.Panicln(args...)
	case log.FatalLevel:
		t.Fatalln(args...)
	case log.ErrorLevel:
		t.Errorln(args...)
	case log.WarnLevel:
		t.Warningln(args...)
	case log.InfoLevel:
		t.Infoln(args...)
	case log.DebugLevel:
		t.Debugln(args...)
	}
}

func (t *TDsMgrLog) Panicln(args ...interface{}) {
	t.Panicf("%v", args...)
}

func (t *TDsMgrLog) Fatalln(args ...interface{}) {
	t.Fatalf("%v", args...)
}

func (t *TDsMgrLog) Errorln(args ...interface{}) {
	t.Errorf("%v", args...)
}

func (t *TDsMgrLog) Warningln(args ...interface{}) {
	t.Warningf("%v", args...)
}

func (t *TDsMgrLog) Infoln(args ...interface{}) {
	t.Infof("%v", args...)
}

func (t *TDsMgrLog) Debugln(args ...interface{}) {
	t.Debugf("%v", args...)
}

func (t *TDsMgrLog) Logf(level log.Level, format string, args ...interface{}) {
	switch level {
	case log.PanicLevel:
		t.Panicf(format, args...)
	case log.FatalLevel:
		t.Fatalf(format, args...)
	case log.ErrorLevel:
		t.Errorf(format, args...)
	case log.WarnLevel:
		t.Warningf(format, args...)
	case log.InfoLevel:
		t.Infof(format, args...)
	case log.DebugLevel:
		t.Debugf(format, args...)
	}
}

func (t *TDsMgrLog) Panicf(format string, args ...interface{}) {
	if nil == t {
		return
	}
	t.CurrLineNum++
	if t.ExtLogLevel >= log.PanicLevel {
		log.Panicf(format, args...)
	}
	t.writeToFile(log.PanicLevel, format, args...)
}

func (t *TDsMgrLog) Fatalf(format string, args ...interface{}) {
	if nil == t {
		return
	}
	t.CurrLineNum++
	if t.ExtLogLevel >= log.FatalLevel {
		log.Fatalf(format, args...)
	}
	t.writeToFile(log.FatalLevel, format, args...)
}

func (t *TDsMgrLog) Errorf(format string, args ...interface{}) {
	if nil == t {
		return
	}
	t.CurrLineNum++
	if t.ExtLogLevel >= log.ErrorLevel {
		log.Errorf(format, args...)
	}
	t.writeToFile(log.ErrorLevel, format, args...)
}

func (t *TDsMgrLog) Warningf(format string, args ...interface{}) {
	if nil == t {
		return
	}
	t.CurrLineNum++
	if t.ExtLogLevel >= log.WarnLevel {
		log.Warningf(format, args...)
	}
	t.writeToFile(log.WarnLevel, format, args...)
}

func (t *TDsMgrLog) Infof(format string, args ...interface{}) {
	if nil == t {
		return
	}
	t.CurrLineNum++
	if t.ExtLogLevel >= log.InfoLevel {
		log.Infof(format, args...)
	}
	t.writeToFile(log.InfoLevel, format, args...)
}

func (t *TDsMgrLog) Debugf(format string, args ...interface{}) {
	if nil == t {
		return
	}
	t.CurrLineNum++
	if t.ExtLogLevel >= log.DebugLevel {
		log.Debugf(format, args...)
	}
	t.writeToFile(log.DebugLevel, format, args...)
}

func (t *TDsMgrLog) writeToFile(level log.Level, format string, args ...interface{}) {
	if nil != t && t.FileLogLevel >= level && nil != t.File {
		t.CurrFileLineNum++
		line := fmt.Sprintf("%d\t[%s]\t", t.CurrFileLineNum, level.String())
		line += fmt.Sprintf(format, args...) + "\n"
		t.File.WriteString(line)
	}
}
