package coldetect

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	pb "gopkg.in/cheggaaa/pb.v1"

	log "github.com/sirupsen/logrus"

	"gitlab.com/Krauze67/flib/stringtools"

	"gitlab.com/Krauze67/coldetect/tools"
)

const (
	cInputsCount = 30
)

//
func (t *TColDetect) ExportTrainNames(dirpath string) error {
	t.mLockInit.Lock()
	defer t.mLockInit.Unlock()
	if !t.bInitialized {
		return errors.New("Not initialized")
	}

	//
	COUNTRY_FILTER := "US|CA|GB|AU"
	var fTrainFirstname, fTrainLastname *os.File
	var err error

	trainFirstname := filepath.Join(dirpath, "firstname_train.csv")
	if fTrainFirstname, err = os.Create(trainFirstname); nil != err {
		return err
	}
	trainLastname := filepath.Join(dirpath, "lastname_train.csv")
	if fTrainLastname, err = os.Create(trainLastname); nil != err {
		return err
	}

	for _, ci := range t.Countries {
		log.Infof("Processing country: %s", ci.CountryName)
		var fIsMatchedCountry float64
		if strings.Contains(COUNTRY_FILTER, ci.CountryName) {
			fIsMatchedCountry = 1
			log.Info(" country is in filters")
		}
		log.Info("    saving firstnames\n")
		bar := pb.StartNew(len(ci.FirstNames))
		for firstname, _ := range ci.FirstNames {
			bar.Increment()
			if !stringtools.IsLatinOnly(firstname, latinViewAlsoAllowedSet) {
				continue
			}
			first := tools.Str2vec(firstname)
			nameAfloat := tools.Vec2afloat(first)

			line := ""
			for _, valF := range nameAfloat {
				line += fmt.Sprintf("%.0f,", valF)
			}
			line += fmt.Sprintf("%01.0f\n", fIsMatchedCountry)
			fTrainFirstname.WriteString(line)
		}
		bar.FinishPrint("done")

		log.Info("    saving lastnames\n")
		barLast := pb.StartNew(len(ci.LastNames))
		for lastname, _ := range ci.LastNames {
			barLast.Increment()
			if !stringtools.IsLatinOnly(lastname, latinViewAlsoAllowedSet) {
				continue
			}
			last := tools.Str2vec(lastname)
			nameAfloat := tools.Vec2afloat(last)

			line := ""
			for _, valF := range nameAfloat {
				line += fmt.Sprintf("%.0f,", valF)
			}
			line += fmt.Sprintf("%01.0f\n", fIsMatchedCountry)
			fTrainLastname.WriteString(line)
		}
		barLast.FinishPrint("done")
	}

	fTrainFirstname.Close()
	fTrainLastname.Close()
	return nil
}

func (t *TColDetect) ExportFullnames(dirpath string) error {
	t.mLockInit.Lock()
	defer t.mLockInit.Unlock()
	if !t.bInitialized {
		return errors.New("Not initialized")
	}

	//
	COUNTRY_FILTER := "US|CA|GB|AU"
	var fTrainFullName *os.File
	var err error
	trainFullName := filepath.Join(dirpath, "full_name_train.csv")
	if fTrainFullName, err = os.Create(trainFullName); nil != err {
		return err
	}

	for _, ci := range t.Countries {
		log.Infof("Processing country: %s", ci.CountryName)
		var fIsMatchedCountry float64
		if strings.Contains(COUNTRY_FILTER, ci.CountryName) {
			fIsMatchedCountry = 1
		}
		for firstname, _ := range ci.FirstNames {
			if !stringtools.IsLatinOnly(firstname, latinViewAlsoAllowedSet) {
				continue
			}
			first := tools.Str2vec(firstname)
			for lastname, _ := range ci.LastNames {
				if !stringtools.IsLatinOnly(lastname, latinViewAlsoAllowedSet) {
					continue
				}
				last := tools.Str2vec(lastname)

				fullNameVec := first + last
				nameAfloat := tools.Vec2afloat(fullNameVec)
				line := ""
				for _, valF := range nameAfloat {
					line += fmt.Sprintf("%.0f,", valF)
				}
				line += fmt.Sprintf("%01.0f\n", fIsMatchedCountry)
				fTrainFullName.WriteString(line)
			}
		}
	}
	fTrainFullName.Close()
	return nil
}

//
func (t *TColDetect) ExportTrainNamesRunes(dirpath string) error {
	t.mLockInit.Lock()
	defer t.mLockInit.Unlock()
	if !t.bInitialized {
		return errors.New("Not initialized")
	}

	//
	COUNTRY_FILTER := "US|CA|GB|AU"
	var fTrainFirstname, fTrainLastname *os.File
	var err error

	trainFirstname := filepath.Join(dirpath, "firstname_train.csv")
	if fTrainFirstname, err = os.Create(trainFirstname); nil != err {
		return err
	}
	trainLastname := filepath.Join(dirpath, "lastname_train.csv")
	if fTrainLastname, err = os.Create(trainLastname); nil != err {
		return err
	}

	for _, ci := range t.Countries {
		log.Infof("Processing country: %s", ci.CountryName)
		var fIsMatchedCountry float64
		if strings.Contains(COUNTRY_FILTER, ci.CountryName) {
			fIsMatchedCountry = 1
			log.Info(" country is in filters")
		}
		log.Info("    saving firstnames\n")
		bar := pb.StartNew(len(ci.FirstNames))
		for firstname, _ := range ci.FirstNames {
			bar.Increment()
			firstnameUp := strings.ToUpper(firstname)
			if !stringtools.IsLatinOnly(firstnameUp, latinViewAlsoAllowedSet) {
				continue
			}
			nameAfloat := tools.StrToNFloats(firstnameUp, cInputsCount)
			line := ""
			for _, valF := range nameAfloat {
				line += fmt.Sprintf("%.0f,", valF)
			}
			line += fmt.Sprintf("%01.0f\n", fIsMatchedCountry)
			fTrainFirstname.WriteString(line)
		}
		bar.FinishPrint("done")

		log.Info("    saving lastnames\n")
		barLast := pb.StartNew(len(ci.LastNames))
		for lastname, _ := range ci.LastNames {
			lastnameUp := strings.ToUpper(lastname)
			barLast.Increment()
			if !stringtools.IsLatinOnly(lastnameUp, latinViewAlsoAllowedSet) {
				continue
			}
			nameAfloat := tools.StrToNFloats(lastnameUp, cInputsCount)
			line := ""
			for _, valF := range nameAfloat {
				line += fmt.Sprintf("%.0f,", valF)
			}
			line += fmt.Sprintf("%01.0f\n", fIsMatchedCountry)
			fTrainLastname.WriteString(line)
		}
		barLast.FinishPrint("done")
	}

	fTrainFirstname.Close()
	fTrainLastname.Close()
	return nil
}
