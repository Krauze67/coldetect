package coldetect

import (
	"bufio"
	"errors"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"

	"gitlab.com/Krauze67/flib/stringtools"

	log "github.com/sirupsen/logrus"

	"encoding/csv"

	"io"

	yaml "gopkg.in/yaml.v2"
)

const (
	subDirFirstName               = "firstname"
	subDirLastname                = "lastname"
	subDirMidname                 = "middlename"
	subFileCountryInfoSpecial     = "countryinfo.yaml"
	cDefDirNameDatasets           = "datasets"
	cDefDirNameDatasetsExamples   = "ds-examples"
	subFileISO3166                = "allcountriesISO3166.csv"
	subFileISO3166FieldsCount     = 8
	subFileISO3166FieldsIdxAlpha2 = 3

	tblFieldNameProp100k  = "PROP100K"
	tblFieldNameName      = "NAME"
	tblSep                = ","
	namesFieldDelimiter   = "|"
	fileCommentStringChar = '#'
	//tblDefNameIdx        = 0

	cProp100KPotentialError = 6600

	latinViewAlsoAllowedSet = " -'`\t."
)

// TNameInfo contains a name(s) and their count by 100k
type TNameInfo struct {
	Name     []string
	Prop100k float64
	//Female   float64
	//Male     float64
}

// TCountryInfoSpecial contains all statistics data by country's names
type TCountryInfoSpecial struct {
	CountryWeight           float64 `yaml:"CountryWeight"`
	CoeffWeightName         float64 `yaml:"CoeffWeightName"`
	CoeffWeightSurname      float64 `yaml:"CoeffWeightSurname"`
	CoeffWeightMidname      float64 `yaml:"CoeffWeightMidname"`
	MidnameEqualToFirstname bool    `yaml:"MidnameEqualToFirstname"`
}

// TCountryInfo contains all statistics data by country's names
type TCountryInfo struct {
	CountryName        string
	CountrySpecialInfo TCountryInfoSpecial
	FirstNames         map[string]*TNameInfo
	MiddleNames        map[string]*TNameInfo
	LastNames          map[string]*TNameInfo
}

// TCountryInfoISO3166 contains all country's data loaded from ISO-3166 record
type TCountryInfoISO3166 struct {
	CountryDetails map[string]string
}

func (t *TColDetect) loadDataSets(dirpath string) error {
	log.Debugf("TColDetect->loadDataSets(%s) called", dirpath)
	if nil == t {
		return errors.New("loadDataSets method called with nil 'this' pointer")
	}

	fulldirpath := dirpath
	if 0 == len(dirpath) {
		log.Debugf("TColDetect->loadDataSets(%s) DS path is empty - try to load default %s from working directory", dirpath, cDefDirNameDatasets)
		def_dataset_path, _ := filepath.Abs(cDefDirNameDatasets)
		_, err := os.Stat(def_dataset_path)
		if err != nil {
			log.Warnf("TColDetect->loadDataSets(%s) no default datasets directory %s. Loading examples DS from package...", dirpath, def_dataset_path)
			_, thisfilename, _, _ := runtime.Caller(0)
			fulldirpath = filepath.Join(path.Dir(thisfilename), cDefDirNameDatasetsExamples)
			log.Debugf("TColDetect->loadDataSets(%s) DS path is empty - built path to examples dataset: %s", dirpath, fulldirpath)
		} else {
			log.Debugf("TColDetect->loadDataSets(%s) default datasets directory %s found in working directory", dirpath, def_dataset_path)
			fulldirpath = def_dataset_path
		}
	}
	if !filepath.IsAbs(fulldirpath) {
		fulldirpath, _ = filepath.Abs(fulldirpath)
	}

	log.Debugf("TColDetect->loadDataSets(%s) check dir stat: %s", dirpath, fulldirpath)
	_, err := os.Stat(fulldirpath)
	if err != nil {
		log.Errorf("TColDetect->loadDataSets(%s) check dir[%s] stat failed: %s ", dirpath, fulldirpath, err.Error())
		return err
	}

	log.Debugf("TColDetect->loadDataSets(%s) loading ISO3166 info[%s]... ", dirpath, fulldirpath)
	err = t.loadISO3166Info(fulldirpath)
	if err != nil {
		log.Errorf("TColDetect->loadDataSets(%s) loadISO3166Info failed with error: %s ", dirpath, err.Error())
		err = nil
	}

	t.Countries = nil
	files, _ := ioutil.ReadDir(fulldirpath)
	for _, d := range files {
		if d.IsDir() {
			dirname_first_rune := ([]rune(strings.ToUpper(d.Name())))[0]
			if !strings.ContainsRune(stringtools.EnUpper, dirname_first_rune) {
				log.Debugf("Directory %s skipped because name first character '%v' is not from english alphabet", d.Name(), dirname_first_rune)
				continue
			}
			country := &TCountryInfo{
				CountryName: d.Name(),
				FirstNames:  make(map[string]*TNameInfo),
				LastNames:   make(map[string]*TNameInfo),
				MiddleNames: make(map[string]*TNameInfo),
			}

			//Load country-special info:
			country.CountrySpecialInfo, err = t.loadCountryInfo(filepath.Join(fulldirpath, d.Name(), subFileCountryInfoSpecial))
			if nil != err {
				log.Errorf("Error [%v] loading special info of country %s", err, country.CountryName)
				continue
			}

			err = t.loadSubDataSet_prop100k(filepath.Join(fulldirpath, d.Name(), subDirFirstName), &country.FirstNames)
			if nil != err {
				log.Errorf("Error [%v] loading %s dataset info of country %s", err, subDirFirstName, country.CountryName)
				continue
			}
			err = t.loadSubDataSet_prop100k(filepath.Join(fulldirpath, d.Name(), subDirLastname), &country.LastNames)
			if nil != err {
				log.Errorf("Error [%v] loading %s dataset info of country %s", err, subDirLastname, country.CountryName)
				continue
			}

			strDsMidNames := subDirMidname
			if country.CountrySpecialInfo.MidnameEqualToFirstname {
				strDsMidNames = subDirFirstName
			}
			err = t.loadSubDataSet_prop100k(filepath.Join(fulldirpath, d.Name(), strDsMidNames), &country.MiddleNames)
			if nil != err {
				log.Errorf("Error [%v] loading Middle-names dataset(%s) info of country %s", err, strDsMidNames, country.CountryName)
				continue
			}
			t.Countries = append(t.Countries, country)
		}
	}

	return nil
}

// GetISO3166Info returns a map of key-value of according country's ISO3166 record
func (t *TColDetect) GetISO3166Info(countryAlpha2 string) map[string]string {
	if nil == t {
		log.Errorf("TColDetect->GetISO3166Info(%s) internal error - nil 't' pointer", countryAlpha2)
		return nil
	}

	if val, ok := t.CountriesISO3166[countryAlpha2]; ok {
		log.Debugf("TColDetect->GetISO3166Info(%s) - found", countryAlpha2)
		return val.CountryDetails
	}

	log.Errorf("TColDetect->GetISO3166Info(%s) - country has not been found", countryAlpha2)
	return nil
}

func (t *TColDetect) loadISO3166Info(dsdirpath string) error {
	if nil == t {
		return errors.New("LoadDataSets method called with nil 'this' pointer")
	}

	var err error
	f, err := os.Open(filepath.Join(dsdirpath, subFileISO3166))
	if err != nil {
		return err
	}

	r := csv.NewReader(bufio.NewReader(f))
	r.Comma = '\t'
	nLineIdx := -1
	var header []string
	for nil == err {
		nLineIdx++
		var record []string
		if record, err = r.Read(); err != nil {
			continue
		}
		if subFileISO3166FieldsCount != len(record) {
			log.Warning("Error loading ISO3166 %s, line %d: got %d fields when %d expected", subFileISO3166, nLineIdx, len(record), subFileISO3166FieldsCount)
			continue
		}
		if 0 == len(header) {
			header = append(header, record...)
			continue
		}
		if len(record) != len(header) {
			log.Warning("Error loading ISO3166 %s, line %d: got %d fields and header contains %d", subFileISO3166, nLineIdx, len(record), len(header))
			continue
		}
		ci3166 := &TCountryInfoISO3166{CountryDetails: make(map[string]string)}
		for i, val := range record {
			ci3166.CountryDetails[header[i]] = val
		}
		t.CountriesISO3166[record[subFileISO3166FieldsIdxAlpha2]] = ci3166
	}

	if err == io.EOF { //assume EOF as OK
		err = nil
	}

	return err
}

func (t *TColDetect) loadCountryInfo(filename string) (TCountryInfoSpecial, error) {
	retVal := TCountryInfoSpecial{
		CountryWeight:      1,
		CoeffWeightName:    1,
		CoeffWeightSurname: 1,
		CoeffWeightMidname: 1,
	}
	fileContent, err := ioutil.ReadFile(filename)
	if err == nil {
		err = yaml.Unmarshal(fileContent, &retVal)
	}
	if retVal.CountryWeight <= 0 {
		retVal.CountryWeight = 1
	}
	if retVal.CoeffWeightName <= 0 {
		retVal.CoeffWeightName = 1
	}
	if retVal.CoeffWeightSurname <= 0 {
		retVal.CoeffWeightSurname = 1
	}
	if retVal.CoeffWeightMidname <= 0 {
		retVal.CoeffWeightMidname = 1
	}

	return retVal, err
}

func (t *TColDetect) loadSubDataSet_prop100k(dirpath string, dataset *map[string]*TNameInfo) error {
	subfiles, _ := ioutil.ReadDir(dirpath)
	for _, subf := range subfiles {
		if !subf.IsDir() {
			fullfilepath := filepath.Join(dirpath, subf.Name())
			//file, err := os.OpenFile(fullfilepath, os.O_RDONLY, 0666)
			file, err := os.Open(fullfilepath)
			if nil != err {
				log.Errorf("TColDetect->loadSubDataSet_prop100k(%s) Error [%v] opening dataset file %s", dirpath, err, fullfilepath)
				continue
			}
			scanner := bufio.NewScanner(bufio.NewReader(file))
			nLine := -1
			idxFldProp100k := -1
			idxFldName := -1
			var nNamesLoaded int64
			for scanner.Scan() {
				nLine++
				line := strings.ToUpper(strings.Trim(scanner.Text(), "\n\r\t "))

				if 0 == len(line) || fileCommentStringChar == line[0] {
					continue
				}

				if -1 == idxFldProp100k { //check headers first:
					if idxFldProp100k = t.getFieldIndex(line, tblFieldNameProp100k, tblSep); -1 == idxFldProp100k {
						//return fmt.Errorf("Error! File %s doesn't contain header's field %s", fullfilepath, tblFieldNameProp100k)
						log.Debugf("TColDetect->loadSubDataSet_prop100k(%s) File %s line %d doesn't contain header's field %s", dirpath, fullfilepath, nLine, tblFieldNameProp100k)
						continue
					}
					if idxFldName = t.getFieldIndex(line, tblFieldNameName, tblSep); -1 == idxFldName {
						//return fmt.Errorf("Error! File %s doesn't contain header's field %s", fullfilepath, tblFieldNameName)
						log.Debugf("TColDetect->loadSubDataSet_prop100k(%s) File %s line %d doesn't contain header's field %s", dirpath, fullfilepath, nLine, tblFieldNameName)
						continue
					}
					log.Debugf("TColDetect->loadSubDataSet_prop100k(%s) File %s line %d header's field indexes got %s:%d, %s:%d", dirpath, fullfilepath, nLine,
						tblFieldNameProp100k, idxFldProp100k,
						tblFieldNameName, idxFldName)
					continue
				}

				namesAll, probab, err := t.getNameAndProbability(line, idxFldName, idxFldProp100k, tblSep)
				if nil != err {
					log.Errorf("TColDetect->loadSubDataSet_prop100k(%s) File %s line %d doesn't contain required fields", dirpath, fullfilepath, nLine)
					continue
				}

				// if probab > cProp100KPotentialError || probab <= 0 {
				// 	log.Warningf("TColDetect->loadSubDataSet_prop100k(%s) File %s line %d[%s] contain PROP100K==%f", dirpath, fullfilepath, nLine, line, probab)
				// }

				log.Debugf("TColDetect->loadSubDataSet_prop100k(%s) File %s line %d name(s) found: %s", dirpath, fullfilepath, nLine, namesAll)
				nameViews := strings.Split(namesAll, namesFieldDelimiter)

				for _, nameView := range nameViews {
					currNameView := strings.Trim(nameView, " \t\n")
					if 0 == len(currNameView) {
						log.Errorf("TColDetect->loadSubDataSet_prop100k(%s) File %s line %d name(s) view [%s] is empty", dirpath, fullfilepath, nLine, nameView)
						continue
					}

					t.addUpdateNameViewInfoVariants(fullfilepath, nLine, currNameView, probab, dataset, &nNamesLoaded)

				} //loop by name views
			} //loop by file

			log.Debugf("TColDetect->loadSubDataSet_prop100k(%s) File %s load finished", dirpath, fullfilepath)
			err = scanner.Err()
			if nil != err {
				log.Errorf("TColDetect->loadSubDataSet_prop100k(%s) File %s load failed: %s", dirpath, fullfilepath, err.Error())
				continue
				//return err
			}

			if 0 == nNamesLoaded { //Check file structure error
				log.Errorf("TColDetect->loadSubDataSet_prop100k(%s) File %s load failed: No names loaded", dirpath, fullfilepath)
				//return fmt.Errorf("Check structure of file %s. NO names has been loaded from it", fullfilepath)
			}

		}
	}
	log.Debugf("TColDetect->loadSubDataSet_prop100k(%s) finished", dirpath)
	return nil
}

func (t *TColDetect) addUpdateNameViewInfoVariants(fullfilepath string, nLine int, nameview string, propval float64, dataset *map[string]*TNameInfo, pNamesLoaded *int64) {

	t.addUpdateNameViewInfo(fullfilepath, nLine, nameview, propval, dataset, pNamesLoaded)
	latinNameView := stringtools.GetLatinView(nameview, latinViewAlsoAllowedSet)
	if 0 < len(latinNameView) && latinNameView != nameview {
		log.Debugf("TColDetect->addUpdateNameViewInfoVariants(...) File %s line %d name view %s has also latin-only view %s - try to add it too",
			fullfilepath, nLine, nameview, latinNameView)
		t.addUpdateNameViewInfo(fullfilepath, nLine, latinNameView, propval, dataset, pNamesLoaded)
	}
}

func (t *TColDetect) addUpdateNameViewInfo(fullfilepath string, nLine int, nameview string, propval float64, dataset *map[string]*TNameInfo, pNamesLoaded *int64) {

	existVal, isExist := (*dataset)[nameview]
	if !isExist {
		ni := &TNameInfo{
			Name:     make([]string, 1),
			Prop100k: propval,
		}
		ni.Name[0] = nameview

		(*pNamesLoaded)++
		(*dataset)[nameview] = ni
		log.Debugf("TColDetect->addUpdateNameViewInfo(...) File %s line %d new name view %s added",
			fullfilepath, nLine, nameview)
	} else { //already exist
		if propval > existVal.Prop100k {
			existVal.Prop100k = propval
			log.Debugf("TColDetect->addUpdateNameViewInfo(...) File %s line %d name view %s already loaded, coeff updated %f->%f",
				fullfilepath, nLine,
				nameview, existVal.Prop100k, propval)
		} else {
			log.Debugf("TColDetect->addUpdateNameViewInfo(...) File %s line %d name view %s already loaded with the same or bigger coeff", fullfilepath, nLine, nameview)
		}
	}
}

func (t *TColDetect) getFieldIndex(headerline string, fieldname string, sep string) int {
	if !strings.Contains(headerline, fieldname) {
		return -1
	}
	//get the index of field in a header
	headers := strings.Split(headerline, sep)
	for n, headerName := range headers {
		if 0 == strings.Compare(headerName, fieldname) {
			return n
		}
	}

	return -1
}

func (t *TColDetect) getNameAndProbability(line string, nameIdx int, probabIdx int, sep string) (string, float64, error) {
	//TODO!!! add NAME field split for sinonyms if present and return []string in this case
	if 0 > nameIdx || 0 > probabIdx {
		return "", 0, errors.New("Invalid field index < 0")
	}

	fields := strings.Split(line, sep)
	if nameIdx >= len(fields) || probabIdx >= len(fields) {
		return "", 0, errors.New("Invalid field index >= count of fields")
	}

	probab, err := strconv.ParseFloat(fields[probabIdx], 64)
	if nil != err {
		return "", 0, err
	}
	return fields[nameIdx], probab, nil
}
